# -*- coding: utf-8 -*-
# import pytest

# from dictlets.dictlet import DummyDictlet
# from dictlets.plugins.dictlet_types.constant import ConstantDictlet
# from dictlets.dictletdictlet import DictletsDictlet
#
#
# @pytest.fixture(scope="session")
# def basic_dictlets():
#
#     d1 = ConstantDictlet(constant_value={"one": 1, "two": 2}, alias="d_one")
#     d2 = ConstantDictlet(constant_value={"ten": 10, "twenty": 20}, alias="d_ten")
#     d3 = ConstantDictlet(
#         constant_value={"hundred": 100, "twohundred": 200}, alias="d_hundred"
#     )
#     d4 = DummyDictlet(init_values={"dummy": {"dummy": 22}}, alias="d_dummy")
#
#     d = DictletsDictlet("test", [d1, d2, d3, d4])
#
#     return d
#
#
# def test_dictlet_query_deps(basic_dictlets):
#
#     assert basic_dictlets.get_query_dependencies("one") == set()
#     assert basic_dictlets.get_query_dependencies("dummy") == {"prop.d_dummy"}
#
#     # assert basic_dictlets.values == {"hundred": 100, "one": 1, "ten": 10, "twenty": 20, "two": 2, "twohundred": 200}
#
#
# def test_dictlet_value(basic_dictlets):
#
#     assert basic_dictlets.retrieve_result() == {
#         "one": 1,
#         "two": 2,
#         "ten": 10,
#         "twenty": 20,
#         "hundred": 100,
#         "twohundred": 200,
#         "dummy": {"dummy": 22},
#     }
