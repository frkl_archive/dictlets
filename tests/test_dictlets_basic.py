#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `dictlets` package."""
# import pytest
#
# from dictlets.dictlets import DictletsDictlet
# from dictlets.dictlet import ConstantDictlet
#
#
# def test_empty_dictlet():
#
#     with pytest.raises(TypeError):
#         d = DictletsDictlet("test")
#
# def test_constant_dictlet():
#     d = DictletsDictlet("test", dictlets=[ConstantDictlet("c_test", {"a": 1, "b": 2})])
#
#     assert d.get_value() == {"a": 1, "b": 2}
#
# def test_empty_target_non_dict():
#
#     d = DictletsDictlet("test", dictlets=[ConstantDictlet("c_test", "test_string_value")])
#     with pytest.raises(TypeError):
#         d.get_value()
#
#
# def test_simple_assembly():
#
#     d1 = ConstantDictlet("c", {"a": 1})
#     d2 = ConstantDictlet("d", {"b": 2})
#
#     d = DictletsDictlet("test", [d1, d2])
#
#     assert d.get_value() == {"a": 1, "b": 2}
#
# def test_simple_assembly_2():
#
#     d1 = ConstantDictlet("d_one", {"one": 1, "two": 2})
#     d2 = ConstantDictlet("d_ten", {"ten": 10, "twenty": 20})
#     d3 = ConstantDictlet("d_hundred", {"hundred": 100, "twohundred": 200})
#
#     d = DictletsDictlet("test", [d1, d2, d3])
#
#     assert d.get_value() == {"hundred": 100, "one": 1, "ten": 10, "twenty": 20, "two": 2, "twohundred": 200}
