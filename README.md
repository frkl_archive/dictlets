[![PyPI status](https://img.shields.io/pypi/status/dictlets.svg)](https://pypi.python.org/pypi/dictlets/)
[![PyPI version](https://img.shields.io/pypi/v/dictlets.svg)](https://pypi.python.org/pypi/dictlets/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/dictlets.svg)](https://pypi.python.org/pypi/dictlets/)
[![Pipeline status](https://gitlab.com/frkl/dictlets/badges/develop/pipeline.svg)](https://gitlab.com/frkl/dictlets/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# dictlets

*Composable dictionaries*


## Description

*Dictlets* allows developers to construct pipelines by declaring tasks which each take a dict as input, and provide
a dict as output, and which can be placed into a dict-structure using a 'target' path.

# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'dictlets' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 dictlets
    git clone https://gitlab.com/frkl/dictlets
    cd <dictlets_dir>
    pyenv local dictlets
    pip install -e .[develop,testing,docs]
    pre-commit install


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
