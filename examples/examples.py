# -*- coding: utf-8 -*-
# from dictlets.dictlet import ConstantDictlet, DummyDictlet, CallableDictlet
# from dictlets.dictletsdictlet import DictletsDictlet

# d1 = ConstantDictlet(constant_value={"one": 1, "two": 2}, alias="d_one", target="ONE")
# d2 = ConstantDictlet(
#     constant_value={"ten": 10, "twenty": 20}, alias="d_ten", target="ONE.TWO"
# )
# d3 = ConstantDictlet(
#     constant_value={"hundred": 100, "twohundered": 200},
#     alias="d_hundred",
#     target="THREE.THREE",
# )
# d4 = DummyDictlet(init_values={"dummy": {"dummy_2": 22}}, alias="d_dummy")

#
# import pp
# # pp(d.get_value())
#
# x = d.get_query_dependencies("dummy")

# d1 = ConstantDictlet(constant_value={"one": 1, "two": 2}, alias="d_one")
# d2 = ConstantDictlet(constant_value={"ten": 10, "twenty": 20}, alias="d_ten")
# d3 = ConstantDictlet(
#         constant_value={"hundred": 100, "twohundred": 200}, alias="d_hundred"
#     )
# d4 = DummyDictlet(init_values={"dummy": {"dummy": 22}}, alias="d_dummy")


# def func():
#
#     return {"this": {"is": {"the": {"result": ["result", "list"]}}}}
#
#
# d4 = CallableDictlet(func, alias="d_func", target="func.result")
#
# d = DictletsDictlet("test", [d1, d2, d3, d4], target="DICTLET.TOW")
#
# dd = DictletsDictlet("test2", [d3, d])

from slugify import slugify

from dictlets.dictletlet import Dictletlet
from dictlets.plugins.dictlet_types.constant import ConstantDictlet

# d1 = ConstantDictlet(input={"one": 1, "two": 2}, alias="d_const_1", target="path_one")
# d2 = ConstantDictlet(input={"three": 3, "four": 4}, alias="d_const_2", target="path_two")
# d3 = ConstantDictlet(input={"three": 3, "four": 4}, alias="d_const_3", target="path_three")
from dictlets.plugins.task_managers.prefect import DaskTaskManager
from dictlets.plugins.task_managers.simple import SimpleTaskManager

d1 = ConstantDictlet(
    input=["server-0", "server-1"], target="machines.servers", alias="server_names"
)
d1_data = {
    "type": "const",
    "id": "constants",
    "input": ["server-0", "server-1"],
    "target": "machines.servers",
}
# d2 = ConstantDictlet(
#     input={"three": 3, "four": 4},
#     target="two",
#     provides={
#         "three": {"__prop__": {"type": "integer"}},
#         "another": {"four": {"__prop__": {"type": "integer"}}},
#     },
#     alias="c_two",
# )
# # d3 = ConstantDictlet(input={"three": 3, "four": 4}, target="four")
# d3 = {"three": 3, "four": 4}
#
#
# def test(input):
#     return {"result": {"my": input}}
#
#
# dc = CallableDictlet(
#     callable=test,
#     input={"one_result": "{{ one.one }}", "{{ three }}": "three"},
#     alias="d_callable_1",
#     target="result_dict",
#     # transform={"ip": "{{ __result__.result.my.one_result }}"},
# )
#
# d = DictletAssembly(alias="d_test", target="path.to.target", dictlets=[d1, d2, d3, dc])
from freckworks.dictlet_types import FrecklesDictlet

context_config = ["dev"]
extra_repos = None


def slugify_register_addresses(input):

    reg_add = input.get("register_addresses", None)
    if reg_add is None:
        return input
    reg_add = slugify(reg_add, separator="_")
    input["register_addresses"] = reg_add
    return input


# multiplier_obj = OrderedDict()
# multiplier_obj["server_name"] = "{{ machines.servers }}"

# multiplier = DictletMultiplier(alias="multi", repl_obj=multiplier_obj)

f_dict_data = {
    "id": "create_lxc",
    "type": "frecklet",
    "alias": "create_lxc_machine",
    "frecklet_name": "lxd-container-running",
    "input": {
        "name": "{{ server_list.server_name }}",
        "source_image": "images:debian/10",
        "image_name": "ladata-base-image",
        "image_server": None,
        "register_addresses": "ip_{{ server_list.server_name }}",
    },
    "context_config": context_config,
    "extra_repos": extra_repos,
    "multiplier": {
        "alias": "server_list",
        "type": "each_list_item",
        "repl_map": {"server_name": "{{ machines.servers }}"},
    },
    "target": "result.servers",
    "preprocess": {
        "alias": "slugify_filter",
        "type": "filter",
        "filter_name": "slugify",
        "filter_args": {"valid_var_name": True},
        "value_names": ["register_addresses"],
    },
    "postprocess": {
        "alias": "template",
        "type": "template",
        "template_obj": {
            "ip": "{{ __result__[__input__.register_addresses].eth0[0] }}"
        },
    },
}

from dictlets.project import *

dpc = DictletsProjectConfig()

dp = DictletsProject.from_config(dpc)

# dp.add_dictlet(d1_data)
# dp.add_dictlet(f_dict_data)
result = dp.query("*")

import pp

pp(result)

# f_dict = FrecklesDictlet(**f_dict_data)

# f_dict= FrecklesDictlet(frecklet_name="lxd-container-running", alias="test", input={"name": "{{ multi.server_name }}", "source_image": "images:debian/10", "image_name": "ladata-base-image", "image_server": None, "register_addresses": "ip_{{ multi.server_name }}"}, context_config=context_config, input_preprocess=slugify_register_addresses, multiplier=multiplier, target="result.servers")

# d = DictletAssembly(dictlets=[f_dict, d1])

# import pp

# pp(d.structure._structure)
#
# pre_res = DaskTaskManager()
# simp_res = SimpleTaskManager()

# result = d.query("*", query_resolver=pre_res)
# result = d.query("*", query_resolver=simp_res)
# import pp
#
# print("RESULT:")
# pp(result)
# pp(d.structure._structure)
# from frutils.properties import PropSchema
#
# provides = {
#     "ip": {"__prop__": {"type": "string", "required": True, "default": "1.1.1.1"}},
#     "service": {
#         "type": "whatever",
#         "other": {
#             "keys": {"test": {"__prop__": {"type": "string", "required": True}}}
#         },
#     },
# }
#
# ps = PropSchema.parse_dict(provides)
#
# import pp
#
# pp(ps.item_list)
