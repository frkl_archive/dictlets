# -*- coding: utf-8 -*-
from dictlets.project import DictletsProject
from dictlets.prototype import DictletPrototypeContext

context = DictletPrototypeContext(
    "dictings", repos=["/home/markus/projects/freckles-dev/dictlets/test_project/repo"]
)

dictlet_configs = [
    {
        "register_var": {
            "dicting": "register_var",
            "vars": {"register_name": "markus", "var_name": "ansible_env.USER"},
            "target": "results.register",
        }
    }
]
dp = DictletsProject.from_folder(
    dictlet_configs,
    "test_project",
    path="~/projects/freckles-dev/dictlets/test_project",
    resolver="prefect",
)

# d = context.create_dictlet_config("register_var", vars={"register_name": "markus", "var_name": "ansible_env.USER"}, target="results")
# d = context.create_dictlet_config("create_lxc", vars={"server_names": "{{ machines.server_names }}"}, target="machines.servers")
# dp.add_dictlet(d)
# d2 = context.create_dictlet_config("constant", vars={"value": ["first", "second", "third", "fourth"]}, target="machines.server_names")
# dp.add_dictlet(d2)

# result = dp.query("*")
result = dp.query("machines.servers")
import pp

pp(result)
