# -*- coding: utf-8 -*-
import copy
import os
from collections import Mapping

from dictlets.defaults import DICTLETS_DEFAULT_ARG_SCHEMA, PROTOTYPE_CONTEXT_SCHEMA
from dictlets.schemas import PROTOTYPE_LOAD_CONFIG
from frutils.config import Cnf
from ting.ting_attributes import TingAttribute
from ting.tings import TingTings


class PrototypeArgsAttribute(TingAttribute):
    def __init__(self):

        pass

    def provides(self):

        return ["args"]

    def requires(self):

        return ["template_keys", "args"]

    def get_attribute(self, ting, attribute_name=None):

        tks = ting.template_keys

        args = {}
        for tk in tks:
            arg = ting.args.get(tk, None)
            if arg is None:
                arg = copy.deepcopy(DICTLETS_DEFAULT_ARG_SCHEMA)
            args[tk] = arg

        return args


class DictletPrototypeContext(object):
    def __init__(self, context_name, repos, cnf=None):

        self._context_name = context_name
        if cnf is None:
            cnf = {}
        if isinstance(cnf, Mapping):
            cnf = Cnf(cnf)
        self._cnf = cnf
        self._context_config = cnf.add_interpreter("context", PROTOTYPE_CONTEXT_SCHEMA)
        self._prototype_index = None

        if not repos:
            raise Exception("No repos provided.")

        self._repos = repos

    @property
    def repos(self):
        return self._repos

    @property
    def prototype_index(self):

        if self._prototype_index is not None:
            return self._prototype_index

        folder_index_conf = []
        used_aliases = []

        for f in self.repos:

            url = f

            alias = os.path.basename(url).split(".")[0]
            i = 1
            while alias in used_aliases:
                i = i + 1
                alias = "{}_{}".format(alias, i)

            used_aliases.append(alias)
            folder_index_conf.append(
                {"repo_name": alias, "folder_url": url, "loader": "prototype_files"}
            )

        self._prototype_index = TingTings.from_config(
            "prototypes",
            folder_index_conf,
            PROTOTYPE_LOAD_CONFIG,
            indexes=["prototype_name"],
        )
        return self._prototype_index

    def get_prototype(self, prototype_name):

        return self.prototype_index.get(prototype_name)

    def get_prototype_names(self):

        return self.prototype_index.keys()

    def create_dictlet_config(
        self,
        prototype_name,
        input,
        id=None,
        dictlet_type=None,
        target=None,
        multiplier=None,
        meta=None,
        preprocess=None,
        postprocess=None,
    ):

        prototype = self.get_prototype(prototype_name)
        if not prototype:
            raise ValueError(
                "No dictlet prototype with name '{}' availble.".format(prototype_name)
            )

        dictlet = prototype.create_dictlet_config(
            input=input,
            id=id,
            dictlet_type=dictlet_type,
            target=target,
            multiplier=multiplier,
            meta=meta,
            preprocess=preprocess,
            postprocess=postprocess,
        )
        return dictlet
