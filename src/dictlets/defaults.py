# -*- coding: utf-8 -*-
import os

from appdirs import AppDirs
from jinja2.nativetypes import NativeEnvironment

from frutils import jinja2_filters, JINJA_DELIMITER_PROFILES

DEFAULT_DICTLETS_JINJA_ENV = NativeEnvironment()
for filter_name, filter_details in jinja2_filters.ALL_FRUTIL_FILTERS.items():
    DEFAULT_DICTLETS_JINJA_ENV.filters[filter_name] = filter_details["func"]

DEFAULT_DICTLETS_ARGS_JINJA_ENV = NativeEnvironment(
    **JINJA_DELIMITER_PROFILES["freckles"]
)
for filter_name, filter_details in jinja2_filters.ALL_FRUTIL_FILTERS.items():
    DEFAULT_DICTLETS_ARGS_JINJA_ENV.filters[filter_name] = filter_details["func"]

DICTLETS_DEFAULT_ARG_SCHEMA = {"required": True, "empty": False}

DICTLETS_DEFAULT_PROTOTYPE_PATH = os.path.join(
    os.path.dirname(__file__), "resources", "prototypes"
)

PROTOTYPE_CONTEXT_SCHEMA = {}

dictlets_app_dirs = AppDirs("dictlets", "frkl")

DEFAULT_CLI_CONFIG = {}

DICTLETS_DEFAULT_PROTOTYPE_PATH = os.path.join(
    os.path.dirname(__file__), "resources", "prototypes"
)

DEFAULT_DICTLETS_TASK_MANAGER = "simple"
DICTLETS_DEFAULT_PROJECT_CONFIG = {
    "project_type": "freckworks",
    "project_name": "freckworks",
    "task_manager": DEFAULT_DICTLETS_TASK_MANAGER,
    "prototype_repos": [DICTLETS_DEFAULT_PROTOTYPE_PATH],
    "freckles_context_config": ["dev"],
}
