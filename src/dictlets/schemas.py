# -*- coding: utf-8 -*-
import copy

from dictlets.defaults import DEFAULT_DICTLETS_ARGS_JINJA_ENV
from frutils import replace_strings_in_obj, dict_merge


def render_prototype_dict(prototype_dict, input):

    # TODO: validate vars
    replaced_prototype_content = replace_strings_in_obj(
        prototype_dict,
        replacement_dict=input,
        jinja_env=DEFAULT_DICTLETS_ARGS_JINJA_ENV,
    )

    return replaced_prototype_content


class CreateDictletConfigMixin(object):
    def __init__(self, *args, **kwargs):
        pass

    def create_dictlet_config(
        self,
        input,
        id=None,
        dictlet_type=None,
        target=None,
        multiplier=None,
        meta=None,
        preprocess=None,
        postprocess=None,
    ):

        prototype_content = copy.deepcopy(self.dict_content)

        if id:
            prototype_content["id"] = id

        if dictlet_type:
            prototype_content["dictlet_type"] = dictlet_type

        if target:
            prototype_content["target"] = target

        if multiplier:
            prototype_content["multiplier"] = multiplier

        if meta:
            if "meta" not in prototype_content.keys():
                prototype_content["meta"] = meta
            else:
                dict_merge(prototype_content, {"meta": meta})

        if preprocess:
            prototype_content["preprocess"] = preprocess

        if postprocess:
            prototype_content["postprocess"] = postprocess

        if not input:
            return prototype_content

        replaced_prototype_content = render_prototype_dict(prototype_content, input)
        return replaced_prototype_content


PROTOTYPE_LOAD_CONFIG = {
    "class_name": "DictletPrototype",
    "attributes": [
        {
            "TemplateKeysAttribute": {
                "source_attr_name": "ting_content",
                "target_attr_name": "template_keys",
                "jinja_env": DEFAULT_DICTLETS_ARGS_JINJA_ENV,
            }
        },
        "PrototypeArgsAttribute",
        {
            "ValueAttribute": {
                "source_attr_name": "dict_content",
                "target_attr_name": "type",
            }
        },
        {
            "ValueAttribute": {
                "source_attr_name": "dict_content",
                "target_attr_name": "meta",
                "default": {},
            }
        },
        {
            "ValueAttribute": {
                "source_attr_name": "meta",
                "target_attr_name": "args",
                "default": {},
            }
        },
    ],
    "ting_id_attr": "prototype_name",
    "mixins": [CreateDictletConfigMixin],
    "loaders": {
        "prototype_files": {
            "class": "ting.tings.FileTings",
            "load_config": {"folder_load_file_match_regex": "\\.dictlet"},
            "attributes": [
                "FileStringContentAttribute",
                {
                    "MirrorAttribute": {
                        "source_attr_name": "filename_no_ext",
                        "target_attr_name": "prototype_name",
                    }
                },
                {
                    "DictContentAttribute": {
                        "dict_name": "dict_content",
                        "source_attr_name": "ting_content",
                    }
                },
            ],
        }
    },
}
