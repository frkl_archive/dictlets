# -*- coding: utf-8 -*-
import uuid

import dpath
from dpath.path import is_glob
from jinja2 import TemplateSyntaxError
from jinja2schema import infer_from_ast, parse
from six import string_types
from slugify import slugify

from dictlets.defaults import DEFAULT_DICTLETS_JINJA_ENV
from frutils import dict_merge
from frutils.frutils import (
    get_leaves_from_dict,
    simple_replace_string_in_obj,
    replace_strings_in_obj,
)


def merge_dictlet_results(results_list):

    result = {}
    for r in results_list:
        if not r.is_retrieved:
            raise ValueError("Result not retrieved yet for dictlet: {}".format(r.id))

        dict_merge(result, r.value, copy_dct=False)

    return result


def assemble_dictlet_id(dictlet_type_name: str, id: str) -> str:
    """Utility method to assemble a *DictletAssembly* id property."""

    return "{}.{}".format(dictlet_type_name, id)


def get_template_schema(template):

    if not template:
        return {}
    schema = infer_from_ast(
        parse(template, jinja2_env=DEFAULT_DICTLETS_JINJA_ENV), ignore_constants=True
    )

    schema.pop("__multiply__", None)

    return schema


def generate_random_id(prefix=None):
    id = str(uuid.uuid4())
    if prefix is None:
        id = slugify("dictlet_" + id, separator="_")
    else:
        id = slugify(prefix + "_" + id, separator="_")

    id = id.replace("-", "_")
    return id


def check_for_duplicates(existing_dictlets, new_dictlet):

    for existing in existing_dictlets:
        if existing.id == new_dictlet.id:
            raise Exception(
                "Can't add dictlet with id '{}': id already used".format(new_dictlet.id)
            )
        # TODO: check if result for target is dict
        if existing.target == new_dictlet.target:
            raise Exception(
                "Can't add dictlet with id '{}': target '{}' already used".format(
                    new_dictlet.id, new_dictlet.target
                )
            )


def query_is_glob(query):

    if not isinstance(query, string_types):
        return False

    return is_glob(query)


def query_dict(query_string, dict_obj):

    if query_is_glob(query_string):
        temp = dpath.util.search(dict_obj, query_string, yielded=True, separator=".")
        query_result = []
        last_token = query_string.split(".")[-1]
        last_token_is_glob = query_is_glob(last_token)

        for r in temp:

            if last_token_is_glob:
                last_result = r[0].split(".")[-1]
                res = {last_result: r[1]}
            else:
                res = r[1]
            query_result.append(res)

    else:
        query_result = dpath.util.get(dict_obj, query_string, separator=".")
    return query_result


def replace_queries_in_obj(obj, repl_dict):

    try:
        result = replace_strings_in_obj(
            obj, replacement_dict=repl_dict, jinja_env=DEFAULT_DICTLETS_JINJA_ENV
        )
        return result
    except (TemplateSyntaxError):
        # this means we most likely have a glob in the template
        pass

    new_obj, _ = simple_replace_string_in_obj(
        obj, "*", "__ALL__", dict_class=dict, repl_dict_keys=True
    )

    schema = get_template_schema(new_obj)
    leaves = list(get_leaves_from_dict(schema))

    query_map = {}
    for index, leaf in enumerate(leaves):
        query = ".".join(leaf)
        if "__ALL__" in query:
            query = query.replace("__ALL__", "*")
            query_map[query] = "__var_{}__".format(index)

    new_repl_map = {}
    new_obj = obj
    for k, v in query_map.items():
        new_obj, replaced = simple_replace_string_in_obj(new_obj, k, "__vars__." + v)
        new_repl_map[v] = query_dict(k, repl_dict)

    new_repl_dict = {"__vars__": new_repl_map}
    dict_merge(new_repl_dict, repl_dict, copy_dct=False)

    result = replace_strings_in_obj(
        new_obj, replacement_dict=new_repl_dict, jinja_env=DEFAULT_DICTLETS_JINJA_ENV
    )
    return result


def get_required_property_paths(obj):

    repl, replaced = simple_replace_string_in_obj(obj, "*", "__ALL__", dict_class=dict)

    # no glob in object
    schema = get_template_schema(repl)
    leaves = get_leaves_from_dict(schema)

    paths = set()
    for leaf in leaves:
        if not leaf:
            continue

        if "__ALL__" in leaf:
            current = []
            for token in leaf:
                if token == "__ALL__":
                    leaf = current
                    break
                else:
                    current.append(token)

        paths.add(".".join(leaf))

    return paths
