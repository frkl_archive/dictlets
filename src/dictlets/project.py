# -*- coding: utf-8 -*-
import abc
import logging
import os
import sys
import uuid
from collections import Sequence, Mapping
from typing import Dict, Type

import six
from ruamel.yaml.comments import CommentedMap
from six import string_types
from stevedore import ExtensionManager

from dictlets.defaults import DICTLETS_DEFAULT_PROTOTYPE_PATH
from dictlets.dictlet_assembly import DictletAssembly
from dictlets.plugins.dictlet_callbacks import DictletCallback
from dictlets.plugins.state_managers import get_state_manager
from dictlets.plugins.task_managers import get_task_manager
from dictlets.prototype import DictletPrototypeContext
from dictlets.schemas import render_prototype_dict
from dictlets.utils import generate_random_id

log = logging.getLogger("dictlets")


def load_project_type(default=None) -> Dict[str, Type]:
    """Loading all dictlet project types and their aliases.

    The result is a dict containing the multiplier alias as key and its type as value.
    """

    log2 = logging.getLogger("stevedore")
    out_hdlr = logging.StreamHandler(sys.stderr)
    out_hdlr.setFormatter(
        logging.Formatter("dictlet project type error -> %(message)s")
    )
    out_hdlr.setLevel(logging.DEBUG)
    log2.addHandler(out_hdlr)
    log2.setLevel(logging.INFO)

    log.debug("Loading dictlet project type...")

    mgr = ExtensionManager(
        namespace="dictlets.project_types",
        invoke_on_load=False,
        propagate_map_exceptions=True,
    )

    if not default:
        default = "dictlets"

    result = {}
    for t in mgr:
        if t.name == "default":
            raise Exception("Project type entrypoint can't be 'default'.")
        if t.name == default:
            result["default"] = t.plugin
        result[t.name] = t.plugin
    return result


def create_project(**kwargs) -> "DictletsProject":

    project_type = kwargs.pop("project_type", None)
    if isinstance(project_type, string_types):
        project_class = load_project_type().get(project_type, None)
        if project_class is None:
            raise ValueError("No project type '{}' registered.".format(project_type))
    elif project_type is None:
        project_class = project_type
    elif not isinstance(project_type, Type) or not issubclass(
        project_type, DictletsProject
    ):
        raise TypeError(
            "Invalid 'project_type' configuration type '{}': must be either string, DictletsProject or a subclass thereof"
        )

    if project_class is None:
        project_class = DictletsProject

    return project_class(**kwargs)


@six.add_metaclass(abc.ABCMeta)
class DictletsProjectListener(object):
    def __init__(self):

        self._listener_id = str(uuid.uuid4())

    def __eq__(self, other):

        if not issubclass(other.__class__, DictletsProjectListener):
            return False

        return self._listener_id == other._listener_id

    def __hash__(self):

        return hash(self._listener_id)

    @abc.abstractmethod
    def project_event(self, project, event_type, data):
        pass


class DictletsProjectManager(DictletsProjectListener):
    def __init__(self, listeners=None):

        self._projects = {}
        if listeners is None:
            listeners = []
        self._listeners = listeners

    def add_listener(self, listener):

        self._listeners.append(listener)

    def project_event(self, project, event_type, data):

        for l in self._listeners:
            l.project_event(project, event_type, data)

    def task_event(self, project, event_type, task):

        for l in self._listeners:
            l.task_event(project, event_type, task)

    def get_projects(self):

        return self._projects

    def add_project(self, project_config):

        if isinstance(project_config, DictletsProject):
            project = project_config
        elif isinstance(project_config, Mapping):
            project = create_project(**project_config)
        else:
            raise TypeError(
                "Can not create dictlets project from type: {}".format(
                    type(project_config)
                )
            )

        if project.id in self._projects.keys():
            raise ValueError("Project id '{}' already in use.")

        for p in self._projects:
            if project._state_manager == p._state_manager:
                raise ValueError(
                    "Can't add project, state manager for new project already in use."
                )

        project.add_listener(self)
        self._projects[project.id] = project

    def get_project(self, project_id):

        p = self._projects.get(project_id, None)

        if p is None:
            raise ValueError("No project with id: {}.".format(project_id))

        return p


class DictletsProjectCallback(DictletCallback):
    def __init__(self, project, project_listeners):

        self._project = project
        if not project_listeners:
            project_listeners = []
        self._project_listeners = project_listeners

    def dictlet_event(self, dictlet, event_type, old_state=None, new_state=None):

        for pl in self._project_listeners:
            data = {
                "dictlet": dictlet.id,
                "old_state": old_state,
                "new_state": new_state,
            }
            pl.project_event(project=self._project, event_type=event_type, data=data)

    def task_event(self, task, event_type):

        for pl in self._project_listeners:
            pl.task_event(self._project, event_type=event_type, task=task)

    def add_listener(self, listener):

        self._project_listeners.append(listener)


class DictletsProject(object):
    def __init__(
        self,
        project_name,
        state_manager,
        task_manager,
        project_id=None,
        prototype_repos=None,
        is_locked=False,
        project_listeners=None,
    ):

        if project_id is None:
            project_id = str(uuid.uuid4())
        self._project_id = project_id

        if project_name is None:
            project_name = os.path.dirname(os.getcwd())
        self._project_name = project_name
        self._state_manager = get_state_manager(state_manager)
        if self._state_manager.is_initialized():
            self._state_manager.load_dictlets()

        self._project_callback = DictletsProjectCallback(
            project=self, project_listeners=project_listeners
        )
        self._root_dictlet = DictletAssembly(
            id="{}_root".format(self._project_name),
            dictlets=list(self._state_manager.dictlets.values()),
            callbacks=[self._project_callback],
        )

        if isinstance(task_manager, string_types):
            task_manager = {"type": task_manager}
        task_manager = get_task_manager(task_manager)
        self._task_manager = task_manager

        def default_result_callback(task):
            self._root_dictlet.set_results(task.result)
            self._project_callback.task_event(task=task, event_type="finished")

        self._task_manager.add_result_callback(default_result_callback)

        if project_listeners is None:
            project_listeners = []
        if not isinstance(project_listeners, Sequence):
            project_listeners = []

        if not prototype_repos:
            prototype_repos = [DICTLETS_DEFAULT_PROTOTYPE_PATH]
        self._prototype_repos = prototype_repos
        self._is_locked = is_locked

        self._dictlet_prototype_context = None

    @property
    def id(self):

        return self._project_id

    def init(self, init_dictlet_configs=None):

        if self.is_initialized:

            raise Exception("Dictlet project state already initialized.")

        self._state_manager.init()

        if not init_dictlet_configs:
            return

        self.add_dictlets(init_dictlet_configs)

    def add_listener(self, listener):

        self._project_callback.add_listener(listener)

    def destroy(self):

        if not self.is_initialized:
            log.warning("Project not initialized, doing nothing...")
            return

        for dictlet_id in self.dictlet_ids:
            self._root_dictlet.remove_dictlet(dictlet_id)

        self._state_manager.destroy()

    def get_dictlet(self, dictlet_id):

        return self._root_dictlet.get_dictlet(dictlet_id)

    @property
    def is_initialized(self):
        return self._state_manager.is_initialized()

    def _ensure_initialized(self):

        if not self.is_initialized:
            raise Exception("Dictlet project state not initialized yet.")

    @property
    def dictlet_prototype_context(self):

        if self._dictlet_prototype_context is not None:
            return self._dictlet_prototype_context

        self._dictlet_prototype_context = DictletPrototypeContext(
            "dictlet_prototypes", repos=self._prototype_repos
        )
        return self._dictlet_prototype_context

    @property
    def structure(self):

        return self._root_dictlet.structure._structure

    def add_dictlets(self, dictlet_configs):

        if not isinstance(dictlet_configs, Sequence):
            dictlet_configs = [dictlet_configs]

        result = []
        for dc in dictlet_configs:

            d = self.add_dictlet(**dc)
            result.append(d)

        return result

    def add_dictlet(
        self,
        prototype=None,
        id=None,
        input=None,
        type=None,
        target=None,
        multiplier=None,
        meta=None,
        preprocess=None,
        postprocess=None,
    ):

        self._ensure_initialized()

        if id is None:
            if prototype:
                prefix = prototype
            elif meta and "alias" in meta.keys():
                prefix = meta["alias"]
            else:
                prefix = None

            id = generate_random_id(prefix=prefix)

        if prototype is None:
            dictlet_config = {
                "id": id,
                "type": type,
                "target": target,
                "multiplier": multiplier,
                "meta": meta,
                "preprocess": preprocess,
                "postprocess": postprocess,
            }
            dictlet_config = render_prototype_dict(dictlet_config, input)
        else:
            dictlet_config = self.dictlet_prototype_context.create_dictlet_config(
                prototype_name=prototype,
                input=input,
                id=id,
                dictlet_type=type,
                target=target,
                multiplier=multiplier,
                meta=meta,
                preprocess=preprocess,
                postprocess=postprocess,
            )

        dictlet = self._state_manager.add_dictlet(dictlet_config=dictlet_config)
        self._root_dictlet.add_dictlet(dictlet)

        return dictlet

    def get_dictlet_values(self, dictlet_ids=None, only_retrieved=True):

        self._ensure_initialized()
        if not only_retrieved:
            raise NotImplementedError()

        if not dictlet_ids:
            dictlet_ids = self.dictlet_ids

        result = self._root_dictlet.get_dictlet_values(dictlet_ids=dictlet_ids)
        return result

    def get_values(self, dictlet_ids=None, wait=False):

        self._ensure_initialized()

        dependency_map, all_retrieved = self._root_dictlet.calculate_dependencies_for_dictlets(
            dictlet_ids=dictlet_ids
        )

        if not all_retrieved:
            task = self._task_manager.retrieve_dictlet_values(dependency_map)
            self._project_callback.task_event(task, "started")
            if wait:
                task.wait()
            return task
        return None

    def query(self, query, wait=False):

        self._ensure_initialized()

        dependency_map, all_retrieved = self._root_dictlet.calculate_dependencies_for_query(
            query_string=query
        )

        if not all_retrieved:
            task = self._task_manager.retrieve_dictlet_values(dependency_map)
            self._project_callback.task_event(task, "started")
            if wait:
                task.wait()
            return task

        result = self._root_dictlet.query(query)
        return result

    def invalidate_dictlets(self, dictlet_ids=None):

        self._ensure_initialized()

        self._root_dictlet.invalidate_dictlets(dictlet_ids=dictlet_ids)

    @property
    def info(self):

        info = CommentedMap()
        info["id"] = self.id
        info["name"] = self._project_name
        info["status"] = "initialized" if self.is_initialized else "not initialized"
        info["locked"] = self._is_locked
        info["state_manager"] = self._state_manager.info
        info["task_manager"] = self._task_manager.info
        info["prototype_repos"] = self._prototype_repos
        if self.is_initialized:
            dictlets = {}
            for d_id in self.dictlet_ids:
                deps = self._root_dictlet.get_dependencies_for_dictlet(d_id)
                dictlets[d_id] = {"dependencies": list(deps)}
            info["dictlets"] = dictlets

        return info

    @property
    def current_state(self):

        self._ensure_initialized()

        return self._root_dictlet.current_state

    @property
    def dictlet_ids(self):

        self._ensure_initialized()

        return self._root_dictlet.dictlet_ids

    @property
    def dictlets(self):

        self._ensure_initialized()

        return self._root_dictlet.dictlets

    def get_tasks(self, min_status=None, max_status=None):

        return self._task_manager.get_tasks(
            min_status=min_status, max_status=max_status
        )

        return self._task_manager.tasks

    def get_task(self, task_id):

        return self._task_manager.get_task(task_id)
