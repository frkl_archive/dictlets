# -*- coding: utf-8 -*-
import marshmallow as ma
from flask import jsonify
from flask.views import MethodView
from flask_smorest import abort


class DictletIdsQuerySchema(ma.Schema):
    dictlet_ids = ma.fields.List(ma.fields.String(), required=False)


class ProjectIdQuerySchema(ma.Schema):
    id = ma.fields.String(required=True)


class TaskResult(ma.Schema):

    success = ma.fields.Boolean(required=True)
    meta = ma.fields.Dict(required=False)


class DictletTaskSchema(ma.Schema):
    id = ma.fields.String(required=True)
    # result = ma.fields.String()
    status = ma.fields.String(attribute="status_string")
    dictlet_ids = ma.fields.List(ma.fields.String)


class DictletRetrieveSchema(ma.Schema):
    background_task = ma.fields.Boolean(required=True)
    values = ma.fields.Dict(required=False)
    task = ma.fields.Nested(DictletTaskSchema, only=["id", "dictlet_ids"])


class DictletInfoSchema(ma.Schema):
    id = ma.fields.String()
    alias = ma.fields.String()
    desc = ma.fields.String()
    status = ma.fields.String()


class ProjectInfoSchema(ma.Schema):
    id = ma.fields.String()
    name = ma.fields.String()
    initialized = ma.fields.Boolean()
    locked = ma.fields.Boolean()
    state_manager = ma.fields.Dict()
    task_manager = ma.fields.Dict()
    prototype_repos = ma.fields.List(ma.fields.String())
    dictlets = ma.fields.Dict()


class DictletConfigSchema(ma.Schema):
    id = ma.fields.Str()
    prototype = ma.fields.Str(required=True)
    target = ma.fields.Str()
    input = ma.fields.Dict()
    multiplier = ma.fields.Dict()
    preprocess = ma.fields.Dict()
    postprocess = ma.fields.Dict()


dictlet_task_schema = DictletTaskSchema()
dictlet_tasks_schema = DictletTaskSchema(many=True)


def generate_resources(project_blp):
    @project_blp.route("/<string:project_id>/tasks")
    class Tasks(MethodView):
        @project_blp.response(dictlet_tasks_schema)
        def get(self, project_id):
            """List all projects tasks."""

            try:
                project = project_blp.get_project(project_id=project_id)
            except (ValueError) as e:
                abort(404, exc=e, message=str(e))

            tasks = project.get_tasks()
            return tasks

    @project_blp.route("/<string:project_id>/state")
    def project_state(project_id):
        """Get the current state of a project."""

        try:
            project = project_blp.get_project(project_id=project_id)
            state = project.current_state
            return jsonify(state)
        except (ValueError) as e:
            abort(404, exc=e, message=str(e))

    @project_blp.route("/<string:project_id>/dictlets/invalidate", methods=["POST"])
    @project_blp.arguments(DictletIdsQuerySchema, location="json", as_kwargs=True)
    @project_blp.response()
    def invalidate(project_id, dictlet_ids=None):
        """Invalidate one or several dictlet values.

        If no query data is provided, the values of all dictlets will be invalidated.
        """

        try:
            project = project_blp.get_project(project_id)
            project.invalidate_dictlets(dictlet_ids=dictlet_ids)

            return {"success": True}
        except (ValueError) as ve:
            abort(404, exc=ve, message=str(ve))
        except (Exception) as e:
            abort(500, exc=e, message=(str(e)))

    @project_blp.route("/<string:project_id>/values", methods=["POST"])
    @project_blp.arguments(DictletIdsQuerySchema, location="json", as_kwargs=True)
    @project_blp.response(DictletRetrieveSchema)
    def values(project_id, dictlet_ids=None):
        """Get the value for one, many or all dictlets of a project.

        If no query data is provided, the values of all dictlets will be retrieved.

        If one or several of the required dictlets are not populated with values yet, a background task will be kicked
        off to retrieve them, and a task id will be provided to check on the status of that task.


        If all of the requested dictlets have a valid values, in the  response object the 'background_task' key is set
        to 'false', and the 'values' key will be populated with dictlet_id/dictlet_value pairs, and no 'task' key will be present.


        If one or several dictlets have invalid values, 'background_task' will be 'true', the 'values' key
        will be populated with the dictlet values that are available right now, and the 'task' key will have a
        reference to the background task that is retrieving the missing values.
        """

        try:
            task = project_blp.get_project(project_id).get_values(
                dictlet_ids=dictlet_ids, wait=False
            )

            values = project_blp.get_project(project_id).get_dictlet_values(
                dictlet_ids=dictlet_ids
            )

            result = {"background_task": bool(task), "values": values}
            if task:
                result["task"] = task
            return result

        except (ValueError) as ve:
            abort(404, exc=ve, message=str(ve))
        except (Exception) as e:
            abort(500, exc=e, message=str(e))

    @project_blp.route("/")
    class Projects(MethodView):
        @project_blp.response(ProjectInfoSchema(many=True))
        def get(self):
            """List all available projects."""

            projects = project_blp.get_projects()
            project_infos = [i.info for i in projects.values()]

            return project_infos

    @project_blp.route("/<string:project_id>")
    class ProjectById(MethodView):
        @project_blp.response(ProjectInfoSchema())
        def get(selfself, project_id):
            """Get information and metadata about a dictlets project."""

            try:
                project = project_blp.get_project(project_id=project_id)
                return project.info
            except (ValueError) as e:
                abort(404, exc=e, message=str(e))

    @project_blp.route("/<string:project_id>/dictlets", methods=["GET", "POST"])
    class Dictlets(MethodView):
        @project_blp.response(DictletInfoSchema(many=True))
        def get(self, project_id):
            """Retrieve a list of dictlets for a project.
            """

            try:
                project = project_blp.get_project(project_id=project_id)
            except (ValueError) as e:
                abort(404, exc=e, message=str(e))

            dictlets_info = [i.info for i in project.dictlets]
            return dictlets_info

        @project_blp.arguments(DictletConfigSchema, location="json")
        @project_blp.response(DictletInfoSchema(), code=201)
        def post(self, dictlet_config, project_id):
            """Add a new dictlet to a project."""

            try:
                project = project_blp.get_project(project_id)
                project.add_dictlet(**dictlet_config)
            except (ValueError) as ve:
                abort(404, exc=ve, message=str(ve))
            except (Exception) as e:
                abort(500, exc=e, message=str(e))

    @project_blp.route(
        "/<string:project_id>/dictlets/<string:dictlet_id>", methods=["GET"]
    )
    class DictletsById(MethodView):
        """Project dictlet details.
        """

        # @project_blp.route("/<string:project_id>/dictlet/<string:dictlet_id>", methods=["GET"])
        @project_blp.response(DictletInfoSchema())
        def get(self, project_id, dictlet_id):
            """Get details for a specific dictlet within a project.
            """

            try:
                project = project_blp.get_project(project_id=project_id)
                dictlet = project.get_dictlet(dictlet_id)
                return dictlet.info
            except (ValueError) as e:
                abort(404, exc=e, message=str(e))
