# -*- coding: utf-8 -*-
import logging
import threading
import time

import socketio
from flask import request
from flask_socketio import Namespace, join_room
from six import string_types

from dictlets.project import DictletsProjectListener
from dictlets.service.projects.resources import dictlet_task_schema

log = logging.getLogger("dictlets")


class SocketIODictletsNamespace(Namespace):
    def on_connect(self):
        print("CONNECTED FROM: {}".format(request.args))

    def on_disconnect(self):

        print("DISCONNECTED: {}".format(request.args))

    def on_join(self, rooms):

        if isinstance(rooms, string_types):
            rooms = [rooms]

        for r in rooms:
            join_room(r)

        print("JOINED: {} - {}".format(request.args, rooms))

    def on_leave(self):

        print("LEFT: {}".format(request.args))

    def on_state_changed(self, data):
        print("received: state changed")
        project_id = data.get("project_id", None)
        if project_id is None:
            log.warning(
                "No project_id specified for state change, not sending event..."
            )
            return

        self.emit(
            "state_changed",
            data,
            namespace=self.namespace,
            room="project_" + project_id,
        )

    def on_task_started(self, data):

        print("received: task started")
        import pp

        pp(data)
        project_id = data.get("project_id", None)
        if project_id is None:
            log.warning(
                "No project_id specified for state change, not sending event..."
            )
            return

        print("emiiting: task_started")
        self.emit(
            "task_started", data, namespace=self.namespace, room="project_" + project_id
        )

    def on_task_finished(self, data):

        print("received: task finished")
        import pp

        pp(data)
        project_id = data.get("project_id", None)
        if project_id is None:
            log.warning(
                "No project_id specified for state change, not sending event..."
            )
            return

        print("emiiting: task_ finished")
        self.emit(
            "task_finished",
            data,
            namespace=self.namespace,
            room="project_" + project_id,
        )


class SocketIOClientCallback(DictletsProjectListener):
    def __init__(self, namespace):

        super(SocketIOClientCallback, self).__init__()
        self._namespace = namespace
        self._sio = socketio.Client(
            reconnection_attempts=0, logger=False, engineio_logger=False
        )

        def init_connect():
            time.sleep(0.2)
            self.connect()

        init_connect()
        # t = threading.Thread(target=init_connect)
        # t.start()

    def connect(self, task=None):

        if self._sio.connected:
            return None

        def wrap():

            if not self._sio.connected:

                try:
                    self._sio.connect(
                        "http://localhost:8000",
                        namespaces=[self._namespace],
                        transports=["websocket"],
                    )
                except (Exception) as e:
                    log.error(e)

            if task is not None:
                task()

        t = threading.Thread(target=wrap)
        t.start()

    def emit_event(self, project_id, event_type, data=None):
        def wrap():
            temp = {
                "project_id".format(project_id): project_id,
                "event_type": event_type,
            }
            if data is not None:
                temp["data"] = data
            self._sio.emit(event_type, data=temp, namespace=self._namespace)

        if self._sio.connected:
            wrap()
        else:
            self.connect(task=wrap)

    def project_event(self, project, event_type, data):

        if event_type == "state_changed":
            self.emit_event(project.id, event_type=event_type, data=data)

    def task_event(self, project, event_type, task):
        task_data = dictlet_task_schema.dump(task)
        self.emit_event(project.id, event_type="task_" + event_type, data=task_data)
