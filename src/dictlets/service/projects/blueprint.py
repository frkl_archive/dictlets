# -*- coding: utf-8 -*-

import logging

from flask_smorest import Blueprint

from dictlets.project import DictletsProjectManager
from dictlets.service.projects.resources import generate_resources
from dictlets.service.projects.socket_io import (
    SocketIODictletsNamespace,
    SocketIOClientCallback,
)

log = logging.getLogger("dictlets")


class DictletsProjectBlueprint(Blueprint):
    def __init__(self, *args, **kwargs):
        """asdfasdfs

        asfsdfasdfdf

        """

        super(DictletsProjectBlueprint, self).__init__(*args, **kwargs)
        self._projects_manager = DictletsProjectManager()
        self._socketio_namespace = "/projects"
        self._socketio = None

        self._socketio_callback = None

    @property
    def project_manager(self):
        return self._projects_manager

    def get_project(self, project_id):

        return self.project_manager.get_project(project_id)

    def get_projects(self):

        return self.project_manager.get_projects()

    def register(self, app, options, first_registration=False):
        try:

            self._socketio = app.socketio
            namespace = SocketIODictletsNamespace(self._socketio_namespace)
            self._socketio.on_namespace(namespace)

            self._socketio_callback = SocketIOClientCallback(
                namespace=self._socketio_namespace
            )
            self._projects_manager.add_listener(self._socketio_callback)
            projects_config = app.config["DICTLET_PROJECTS"]

            if projects_config is None:
                projects_config = []

            for pc in projects_config:
                self._projects_manager.add_project(pc)

            generate_resources(self)

            super(DictletsProjectBlueprint, self).register(
                app, options, first_registration
            )

        except (Exception) as e:
            log.error(e)
            raise e


# from .models import ma

# ma.init_app(project_blp)

#
# @project_blp.route("/")
# def view_base():
#
#     if app.project.is_initialized:
#         current_state = app.project.current_state
#         dictlet_infos = dictlets_info_schema.dump(
#             [i.info for i in app.project.dictlets]
#         )
#         return jsonify(
#             {"initialized": True, "state": current_state, "dictlets": dictlet_infos}
#         )
#     else:
#         return jsonify({"initialized": False})
#
#
# @project_blp.route("/state")
# def state():
#
#     if not app.project.is_initialized:
#         return jsonify({})
#
#     current_state = app.project.current_state
#     return jsonify(current_state)
#
#
# @project_blp.route("/retrieve", methods=["POST"])
# def retrieve():
#
#     if not request.data:
#         data = None
#     else:
#         data = request.json
#
#     if not data:
#         data = None
#     if isinstance(data, string_types):
#         data = [data]
#
#     try:
#         task = app.project.retrieve(dictlet_ids=data)
#         state = app.project.current_state
#         return jsonify({"success": True, "state": state})
#
#     except (Exception) as e:
#         return jsonify({"success": False, "error": str(e)})
#
#
# @project_blp.route("/query", methods=["POST"])
# def query():
#
#     if not request.data:
#         data = None
#     else:
#         data = request.json
#         if "query" not in data.keys():
#             return jsonify({"success": False, "error": "No key 'query' provided."})
#
#     if not data:
#         query = "*"
#     else:
#         query = data["query"]
#
#     try:
#         result = app.project.query(query=query)
#         return jsonify({"success": True, "result": result, "query": query})
#
#     except (Exception) as e:
#         import traceback
#
#         traceback.print_exc()
#         return jsonify({"success": False, "error": str(e)})
#
#
# @project_blp.route("/info")
# def project_info():
#
#     response = project_info_schema.dump(app.project.info)
#     return jsonify(response)
#
#
# # @project_blueprint.route("/dictlets")
# # def dictlets():
# #
# #     response = dictlets_info_schema.dump([i.info for i in app.project.dictlets])
# #     return jsonify(response)
# #
# #
# # @project_blueprint.route("/dictlet", methods=["POST"])
# # def dictlet():
# #
# #     data = request.json
# #
# #     if not isinstance(data, Sequence):
# #         data = [data]
# #
# #     dc = dictlet_configs_schema.load(data)
# #
# #     try:
# #         app.project.add_dictlets(dc)
# #         state = app.project.current_state
# #         return jsonify({"success": True, "state": state})
# #     except (Exception) as e:
# #         return jsonify({"success": False, "error": str(e)})
#
#
# @project_blp.route("/invalidate", methods=["POST"])
# def invalidate():
#
#     if not request.data:
#         data = None
#     else:
#         data = request.json
#
#     if not data:
#         data = None
#     if isinstance(data, string_types):
#         data = [data]
#     try:
#         app.project.invalidate_dictlets(data)
#         state = app.project.current_state
#         return jsonify({"success": True, "state": state})
#     except (Exception) as e:
#         return jsonify({"success": False, "error": str(e)})
#
#
# @project_blp.route("/init", methods=["POST"])
# def init_project():
#
#     data = request.json
#
#     if not isinstance(data, Sequence):
#         data = [data]
#
#     dc = dictlet_configs_schema.load(data)
#
#     try:
#         app.project.init(init_dictlet_configs=dc)
#         state = app.project.current_state
#         return jsonify({"success": True, "state": state})
#     except (Exception) as e:
#         return jsonify({"success": False, "error": str(e)})
#
#
# @project_blp.route("/destroy", methods=["POST"])
# def destroy_project():
#
#     try:
#         app.project.destroy()
#         return jsonify({"success": True})
#     except (Exception) as e:
#         import traceback
#
#         traceback.print_exc()
#         print(e)
#         return jsonify({"success": False, "error": str(e)})
