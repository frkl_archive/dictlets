# -*- coding: utf-8 -*-
class DictletsServiceConfig(object):

    DICTLET_PROJECTS = []
    DEBUG = False
    OPENAPI_VERSION = "3.0.2"
    OPENAPI_URL_PREFIX = "/doc"
    OPENAPI_REDOC_PATH = "/redoc"
    OPENAPI_REDOC_VERSION = "next"
    OPENAPI_SWAGGER_UI_PATH = "/swagger"
    OPENAPI_SWAGGER_UI_URL = "https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.19.5/"
    API_SPEC_OPTIONS = {"x-internal-id": "2"}
