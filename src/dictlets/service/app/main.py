# -*- coding: utf-8 -*-
import logging

from flask import Blueprint

# from dictlets.service.projects.resources import DictletInfoSchema

main = Blueprint("main", __name__)
logger = logging.getLogger()


# @main.route("/")
# def view_base():
#
#     if app.project.is_initialized:
#         current_state = app.project.current_state
#         dictlet_infos = DictletInfoSchema(many=True).dump(
#             [i.info for i in app.project.dictlets]
#         )
#         return jsonify(
#             {"initialized": True, "state": current_state, "dictlets": dictlet_infos}
#         )
#     else:
#         return jsonify({"initialized": False})
