# -*- coding: utf-8 -*-
import logging
import os

from flask import Flask
from flask_cors import CORS
from flask_smorest import Api
from flask_socketio import SocketIO

from dictlets.service.projects.blueprint import DictletsProjectBlueprint

logger = logging.getLogger()

ENV_CONFIG_KEYS = ["DEBUG", "ENV"]

log = logging.getLogger("dictlets")


def create_app(config_dict=None):

    return entrypoint(config_dict=config_dict)


def entrypoint(config_dict=None):

    try:

        app = Flask("dictlets service", static_url_path="", static_folder="static")

        configure_app(app, config_dict)

        # app.spec = generate_apispec(app)

        CORS(app)
        logging.getLogger("socketio").setLevel(logging.ERROR)
        logging.getLogger("engineio").setLevel(logging.ERROR)
        socketio = SocketIO(app, cors_allowed_origins="*", async_mode="eventlet")
        app.socketio = socketio

        configure_logging(debug=app.config["DEBUG"])

        app.config.update({})
        api = Api(app)

        from dictlets.service.app.main import main

        # from dictlets.service.projects.blueprint import project_blp

        blp_desc = """Rest API to manage 'dictlets' projects.

        """

        project_blp = DictletsProjectBlueprint(
            "projects",
            __name__,
            static_folder="static",
            template_folder="templates",
            description=blp_desc,
        )

        try:
            api.register_blueprint(project_blp, url_prefix="/project")
        except (Exception) as e:
            log.error(e)
            raise e

        # register blueprints
        app.register_blueprint(main, url_prefix="")
        # app.register_blueprint(project_blp, url_prefix="/project")

        # app.spec.register(DictletResource, endpoint='dictletsresource', blueprint='project')

        import pp

        pp(app.url_map)

        return app
    except (Exception) as e:
        log.error(e)
        raise e


def configure_app(app, config_dict=None):

    logger.info("configuring flask app")

    # load default config
    app.config.from_object("dictlets.service.app.config.DictletsServiceConfig")

    if config_dict:
        app.config.from_mapping(config_dict)

    for key in ENV_CONFIG_KEYS:

        value = os.environ.get(key)
        if value is None:
            continue
        logger.debug("Adding config key: {}".format(key))
        app.config[key] = value


def configure_logging(debug=False):

    root = logging.getLogger()
    h = logging.StreamHandler()
    fmt = logging.Formatter(
        fmt="%(asctime)s %(levelname)s (%(name)s) %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%S",
    )
    h.setFormatter(fmt)

    root.addHandler(h)

    if debug:
        root.setLevel(logging.DEBUG)
    else:
        root.setLevel(logging.INFO)
