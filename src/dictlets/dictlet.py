# -*- coding: utf-8 -*-
import abc
import copy
import logging
import uuid
from collections import OrderedDict
from typing import Dict, Set, Union, Mapping

import dpath
import six

from dictlets.defaults import DEFAULT_DICTLETS_JINJA_ENV
from dictlets.exceptions import DictletValueException, DictletPostprocessException
from dictlets.plugins.dictlet_callbacks import create_callback
from dictlets.plugins.input_preprocessors import (
    input_preprocessor_from_dict,
    DictletInputPreprocessor,
)
from dictlets.plugins.multipliers import multiplier_from_dict, DictletMultiplier
from dictlets.plugins.result_postprocessors import result_postprocessor_from_dict
from dictlets.utils import generate_random_id
from dictlets.utils import merge_dictlet_results
from frutils.doc import Doc
from frutils.frutils import replace_strings_in_obj, dict_merge

log = logging.getLogger("dictlets")


DICTLET_METADATA_DEFAULTS = {"idempotent": False, "bound_to_local": False}


class DictletMetadata(object):
    @classmethod
    def create_metadata(cls, data=None, defaults=None):
        if defaults is None:
            defaults = DICTLET_METADATA_DEFAULTS
        temp = dict_merge(defaults, data, copy_dct=True)
        return DictletMetadata(**temp)

    def __init__(self, **data):

        self._alias = data.get("alias", None)

        doc = data.get("doc", None)
        if isinstance(doc, Mapping):
            doc = Doc(doc)
        if not doc:
            doc = Doc({})
        doc = doc
        self._doc = doc

        properties = data.get("properties", None)
        if properties is None:
            properties = {}
        self._properties = properties

        result_schema = data.get("result", None)
        if result_schema is None:
            result_schema = {}
        self._result = result_schema

        self._args = data.get("args", {})

    @property
    def alias(self):
        return self._alias

    @property
    def args(self):
        return self._args

    @property
    def doc(self):
        return self._doc

    @property
    def properties(self):
        return self._properties

    @property
    def result(self):
        return self._result

    @property
    def is_idempotent(self):
        return self._properties.get("idempotent")

    @property
    def bound_to_local(self):
        return self._properties.get("bount_to_local")


@six.add_metaclass(abc.ABCMeta)
class Dictlet(object):
    """Wrapper object to separate the computation part of a *DictletAssembly* into a discrete object.

    The value itself is determined by calling the '_retrieve' method of this object. For convenience in certain cases,
    if the result of this call is 'None", this parent class will check for the '_value' attribute of the object in
    question, and will use that if non-'None'.

      - *dictlet_id*: the id of the 'parent' *DictletAssembly*
      - *target*: the target path where the retrieved result of this *Dictlet* will be inserted in the overall
            *DictletAssembly* result
      - *postprocess*: a postprocessor to transform the result into a more desirable format
    """

    def __init__(
        self,
        id: str = None,
        input=None,
        target: str = None,
        multiplier: Union[Dict, DictletMultiplier] = None,
        preprocess: Union[Dict, DictletInputPreprocessor] = None,
        postprocess=None,
        callbacks=None,
        meta=None,
    ):

        if callbacks is None:
            callbacks = []
        self._callbacks = callbacks

        self._input = input

        if multiplier and isinstance(multiplier, Mapping):
            multiplier = multiplier_from_dict(multiplier)
        self._multiplier = multiplier

        if preprocess and isinstance(preprocess, Mapping):
            preprocess = input_preprocessor_from_dict(preprocess)
        self._input_preprocess = preprocess

        if postprocess and isinstance(postprocess, Mapping):
            postprocess = result_postprocessor_from_dict(postprocess)
        self._result_postprocessor = postprocess

        self._parent = None
        self._dictlet_type_name = self._get_dictlet_type_name()

        if not id:
            if meta and meta.get("alias", None):
                id = generate_random_id(prefix=meta["alias"])
            else:
                id = generate_random_id(prefix=self.dictlet_type_name)

        self._id = id
        # self._assemble_dictlet_id(self._dictlet_type_name, id)
        if meta is None:
            meta = {}
        if "alias" not in meta.keys():
            meta["alias"] = id
        self._meta = DictletMetadata.create_metadata(data=meta)

        if target is None:
            target = "{}.{}".format(self.dictlet_type_name, self.id)

        if not target:
            raise ValueError(
                "Error creating dictlet '{}': target can't be empty.".format(self.id)
            )

        if not isinstance(target, six.string_types):
            raise TypeError(
                "Invalid type for 'target' argument, needs to be string: {}".format(
                    type(target)
                )
            )
        self._target = target

        self._input_args = None

        self._value_cache = None
        self._is_retrieved = False

        # run-related vars, only important in the context of a run
        self._result = None
        self._run_meta = None
        self._run_skipped = None

    @property
    def id(self) -> str:
        """Returns the id of the *DictletAssembly* this value belongs to."""
        return self._id

    @property
    def alias(self) -> str:
        return self.meta.alias

    @property
    def input(self):
        return self._input

    @property
    def is_retrieved(self) -> bool:

        return self._is_retrieved

    @property
    def meta(self) -> DictletMetadata:
        return self._meta

    @property
    def target(self) -> str:
        """Returns the 'target' attribute of this value."""

        return self._target

    @target.setter
    def target(self, target):
        self._target = target

    @property
    def parent(self) -> str:
        """Returns the id of the parent dictlet (if there is one), or None."""

        return self._parent

    @parent.setter
    def parent(self, parent):

        self._parent = parent

    @property
    def input_args(self):
        if self._input_args is not None:
            return self._input_args

        self._input_args = self._get_input_args()
        return self._input_args

    @property
    def dictlet_type_name(self):
        """Returns the type of this *Dictlet*.

        A dictlet type is used to make it clear to the user what kind of processing is required to get the value. For example,
        'tasks' could be a tasks to indicate a (long-running) task needs to be executed. Or 'secrets', to mean
        that a password needs to be retrieved from a secure location.

        When present in a template, the type is the first part of the templated variable, followed by
        the *Dictlet* alias, e.g. ``tasks.init_system``, or ``secrets.postgres_password``.
        """

        return self._dictlet_type_name

    def add_callback(self, callback):

        callback = create_callback(callback)
        self._callbacks.append(callback)
        self.send_event("callback_added", new_state=callback)

    def remove_callback(self, callback):

        callback = create_callback(callback)
        self._callbacks.remove(callback)
        self.send_event("callback_removed", old_state=callback)

    def send_event(self, event_type, old_state=None, new_state=None):

        for cb in self._callbacks:
            cb.dictlet_event(
                dictlet=self,
                event_type=event_type,
                old_state=old_state,
                new_state=new_state,
            )

    def __eq__(self, other):

        if not issubclass(self.__class__, Dictlet):
            return False

        return self.id == other.id

    def __hash__(self):
        return hash(self.id)

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            if k == "_callbacks":
                setattr(result, k, [])
            else:
                setattr(result, k, copy.deepcopy(v, memo))

        return result

    @abc.abstractmethod
    def _get_input_args(self) -> Set:

        pass

    @abc.abstractmethod
    def _get_dictlet_type_name(self) -> str:
        pass

    def export_dictlet_config(self):

        raise NotImplementedError()

    @property
    def multiplier(self):
        return self._multiplier

    def invalidate(self):

        old_value = self._value_cache

        self._is_retrieved = False
        self._result = None
        self._value_cache = None
        self._run_meta = None
        self._run_skipped = None

        self.send_event("dictlet_invalidated", old_state=old_value)

    def assemble_tasks(self, input_dictlets):

        state = merge_dictlet_results(input_dictlets)

        run_metadata = {"task_name": self.alias}

        if self.multiplier:
            tasks = []
            states = self.multiplier.multiply(state)
            run_metadata["task_batch_total"] = len(states)

            if not states:
                log.warning(
                    "Empty tasklist generated by multiplier '{}' for dictlet: {}".format(
                        self.multiplier.alias, self.alias
                    )
                )

            index = 1
            for extra_target, st in states.items():

                task = copy.deepcopy(self)
                rm = copy.copy(run_metadata)
                rm["task_nr"] = index
                rm["task_name"] = rm["task_name"] + "_" + str(index)
                index = index + 1
                if not self.target:
                    target = extra_target
                else:
                    target = self.target + "." + extra_target
                task.target = target
                task._current_context = st
                task._run_meta = rm
                tasks.append(task)

            return tasks
        else:
            task = copy.deepcopy(self)
            run_metadata["task_batch_total"] = 1
            run_metadata["task_batch_nr"] = 1
            task._run_meta = run_metadata
            task._current_context = state
            return [task]

    def preprocess_input(self, input):

        if not self._input_preprocess:
            return input
        else:
            input = self._input_preprocess.preprocess(input)
            return input

    @property
    def info(self):

        info = OrderedDict()
        info["alias"] = self.alias
        info["desc"] = self.meta.doc.get_short_help(use_help=True)
        info["id"] = self.id
        info["status"] = "retrieved" if self.is_retrieved else "empty"
        info["value"] = self.value if self.is_retrieved else "n/a"

        return info

    def retrieve(self, force: bool = False) -> Dict:

        if force:
            self.invalidate()

        if self.is_retrieved:
            self._run_skipped = True
            return None

        repl = {}
        dict_merge(repl, self._current_context, copy_dct=False)

        input = replace_strings_in_obj(
            self.input, replacement_dict=repl, jinja_env=DEFAULT_DICTLETS_JINJA_ENV
        )
        self.send_event("diclet_retrieve_started", new_state={"input": input})
        input = self.preprocess_input(input)
        self._result = self._retrieve(input)

        self._run_meta = None

        if self._result is None:
            raise DictletValueException(
                self, msg="Could not retrieve a non-null result value."
            )

        if self._result_postprocessor:
            try:
                self._result = self._result_postprocessor.postprocess(
                    input, self._result, self._current_context
                )
            except (Exception) as e:
                raise DictletPostprocessException(
                    msg="Could not postprocess result for dictlet '{}': {}".format(
                        self.id, e
                    ),
                    dictlet=self,
                    postprocessor=self._result_postprocessor,
                    parent=e,
                )

        self.send_event(
            "dictlet_retrieve_finished",
            new_state={"result": self._result, "input": self._input},
        )
        self._run_skipped = False
        self._is_retrieved = True
        return self._result

    @property
    def value(self):
        """Returns the (retrieved) value of this *DicletValue* object."""

        if not self.is_retrieved:
            raise Exception("Value not retrieved yet for dictlet: {}".format(self.id))

        return self._value_cache

    def update_value(self, results):

        old_value = {"value": self._value_cache}

        if not self.multiplier:
            value = results[0]
        else:
            value = {}
            for result in results:
                dict_merge(value, result, copy_dct=False)

        if not self._target:
            self._value_cache = value
        else:
            self._value_cache = {}
            dpath.util.new(self._value_cache, self._target, value, separator=".")

        self._is_retrieved = True
        self.send_event(
            "value_updated", old_state=old_value, new_state={"value": self.value}
        )

    def _retrieve(self, input):
        """Method a *Dictlet* class needs to implement, returning the value (which can be any type) of the *DictletAssembly*.

        This is called as close to when the result is needes as possible, to facilitate distribured
        execution. After the value is returned, the *Dictlet* parent class will move it to the right 'target'
        path within the result dictionary an does some validation against the schema in the provided attribute
        if applicable.
        """

        return None

    def __repr__(self):

        return "[{}: alias:'{}', target='{}']".format(
            self.__class__.__name__, self.alias, self.target
        )


# class DictletQuery(object):
#     def __init__(self, query_string: str, dictlets: Dict[str, Set[str]]):
#
#         self._query_string = query_string
#         self._dictlets = dictlets
#
#         # self._finished_dictlets = []
#         # self._finished_dictlet_ids = set()
#         #
#         # self._unfinished_dictlets = []
#         # self._unfinished_dictlet_ids = set()
#         #
#         # self._inital_state = {}
#         # for dictlet_id, dictlet in dictlets.items():
#         #     if dictlet.is_retrieved:
#         #         self._finished_dictlet_ids.add(dictlet.id)
#         #         self._finished_dictlets.append(dictlet)
#         #         dict_merge(self._inital_state, dictlet.value, copy_dct=False)
#         #     else:
#         #         self._unfinished_dictlet_ids.add(dictlet.id)
#         #         self._unfinished_dictlets.append(dictlet)
#
#     # @property
#     # def initial_state(self):
#     #     return self._inital_state
#     #
#     # @property
#     # def finished_dictlets(self):
#     #     return self._finished_dictlets
#     #
#     # @property
#     # def unfinished_dictlets(self):
#     #     return self._unfinished_dictlets
#
#     @property
#     def dictlets(self):
#         return self._dictlets
#
#     @property
#     def query_string(self):
#         return self._query_string


class DictletState(object):
    def __init__(self, dictlet_id, input, result, value):

        self._id = str(uuid.uuid4())
        self._dictlet_id = dictlet_id
        self._input = input
        self._result = result
        self._value = value

    @property
    def id(self):
        return self._id

    @property
    def dictlet_id(self):
        return self._dictlet_id

    @property
    def input(self):
        return self._input

    @property
    def result(self):
        return self._result

    @property
    def value(self):
        return self._result
