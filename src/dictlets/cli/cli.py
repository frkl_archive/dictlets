# -*- coding: utf-8 -*-
import click

from dictlets.defaults import DICTLETS_DEFAULT_PROJECT_CONFIG
from frutils.frutils_cli import logzero_option
from .wrapper import DictletCli


@click.group()
@logzero_option()
@click.pass_context
def cli(ctx):
    pass


@cli.group(name="debug")
@click.pass_context
def debug(ctx):

    pass


DictletCli.add_dictlets_commands(project_config=DICTLETS_DEFAULT_PROJECT_CONFIG)
