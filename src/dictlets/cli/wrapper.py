# -*- coding: utf-8 -*-
import sys
from typing import Mapping, Sequence

import click
from omegaconf import OmegaConf
from six import string_types

from dictlets.project import create_project
from frkl import VarsType
from frutils import readable, special_dict_to_dict
from frutils.exceptions import handle_exc


class DictletCli(object):
    @classmethod
    def add_dictlets_commands(
        cls, project_config=None, command_map=None, auto_initialize=False
    ):

        if project_config is None:
            project_config = {}
        project = create_project(**project_config)

        if auto_initialize:
            if not project.is_initialized:
                if isinstance(auto_initialize, (Sequence, Mapping)):
                    project.init(init_dictlet_configs=auto_initialize)
                else:
                    project.init()

        cli = DictletCli(dictlet_project=project, command_map=command_map)
        return cli

    def __init__(self, dictlet_project, command_map):

        self._project = dictlet_project
        self._command_map = command_map

        self.create_cli()

    @property
    def project(self):
        return self._project

    def create_cli(self):

        cm = {}
        for command in [
            "init",
            "add-dictlet",
            "info",
            "dictlets",
            "values",
            "query",
            "current-state",
            "invalidate",
            "structure",
            "service",
        ]:
            parent_group = self._command_map.get(
                command, self._command_map.get("__default__", None)
            )
            if parent_group is None:
                continue
            cm[command] = parent_group

        for command_name, parent_group in cm.items():

            if isinstance(parent_group, Mapping):
                name = parent_group["name"]
                parent_group = parent_group["group"]
            else:
                name = command_name

            if command_name == "init":

                @parent_group.command(name=name)
                @click.pass_context
                @handle_exc
                def init(ctx):

                    project = self._project
                    project.init()

            if command_name == "add-dictlet":

                @parent_group.command(name=name)
                @click.option("--prototype", "-p", help="the name of the prototype")
                @click.option("--id", "-i", help="the dictlet id", required=False)
                @click.option(
                    "--target",
                    "-t",
                    help="the target path for the result of this dictlet",
                    required=False,
                )
                @click.argument("vars", type=VarsType(), nargs=-1)
                @click.pass_context
                @handle_exc
                def add_dictlet(ctx, prototype, vars, target=None, id=None):

                    project = self._project
                    if vars:
                        vars = special_dict_to_dict(OmegaConf.merge(*vars))
                    else:
                        vars = {}

                    project.add_dictlet(
                        prototype=prototype, id=id, target=target, input=vars
                    )

            if command_name == "info":

                @parent_group.command(name=name)
                @click.pass_context
                @handle_exc
                def info(ctx):

                    project = self._project
                    click.echo()

                    info = project.info
                    click.echo(readable(info, out="yaml"))

            if command_name == "dictlets":

                @parent_group.command(name=name)
                @click.pass_context
                @handle_exc
                def list_dictlets(ctx):

                    project = self._project
                    click.echo()
                    for dictlet in project.dictlets:
                        click.secho(dictlet.alias, bold=True)
                        click.echo()
                        dictlet.info.pop("alias")
                        click.echo(readable(dictlet.info, out="yaml", indent=2))

            if command_name == "values":

                @parent_group.command(name=name)
                @click.argument("dictlet", nargs=-1)
                @click.pass_context
                @handle_exc
                def run(ctx, dictlet):

                    click.echo()
                    project = self._project
                    project.get_values(dictlet, wait=True)

                    click.echo("Current state:\n")

                    state = project.current_state
                    click.echo(readable(state, out="yaml"))

            if command_name == "query":

                @parent_group.command(name=name)
                @click.option(
                    "--no-run",
                    "-n",
                    help="don't run query, only print dependencies that would run",
                    is_flag=True,
                )
                @click.argument("query_string", nargs=1)
                @click.pass_context
                @handle_exc
                def query(ctx, query_string, no_run):

                    click.echo()
                    if no_run:
                        dependency_dictlet_map, _ = self.project._root_dictlet._assemble_query_dictlet_map(
                            query_string=query_string
                        )

                        for dictlet, deps in dependency_dictlet_map.items():
                            click.echo(dictlet.id)
                            for d in deps:
                                click.echo("  - {}".format(d))

                        sys.exit()

                    project = self._project
                    result = project.query(query_string, wait=True)

                    if isinstance(result, string_types) or not isinstance(
                        result, (Mapping, Sequence)
                    ):
                        click.echo(result)
                    else:
                        click.echo(readable(result, out="yaml"))

            if command_name == "current-state":

                @parent_group.command(name=name)
                @click.pass_context
                @handle_exc
                def current_state(ctx):

                    click.echo()
                    project = self._project
                    state = project.current_state
                    click.echo(readable(state, out="yaml"))

            if command_name == "invalidate":

                @parent_group.command(name=name)
                @click.argument("dictlet", nargs=-1)
                @click.pass_context
                @handle_exc
                def invalidate(ctx, dictlet):

                    click.echo()
                    project = self._project
                    project.invalidate_dictlets(dictlet)

                    click.echo("Current state:\n")

                    state = project.current_state
                    click.echo(readable(state, out="yaml"))

            if command_name == "structure":

                @parent_group.command(name=name)
                @click.pass_context
                @handle_exc
                def structure(ctx):

                    click.echo()
                    project = self._project

                    structure = project.structure

                    click.echo(readable(structure, out="yaml"))

            if command_name == "service":

                @parent_group.command(name=name)
                @click.option(
                    "--debug",
                    "-d",
                    help="start service in debug/develop mode",
                    is_flag=True,
                )
                @click.pass_context
                @handle_exc
                def service(ctx, debug):
                    from dictlets.service.app.core import create_app

                    config = {"DICTLET_PROJECTS": [self.project], "DEBUG": debug}

                    app = create_app(config_dict=config)
                    socketio = app.socketio
                    if not debug:

                        socketio.run(app, port=8000, host="0.0.0.0")
                        # gunicorn_app = GUnicornFlaskApplication(app)
                        # gunicorn_app.run(
                        #     worker_class="gunicorn.workers.sync.SyncWorker",
                        # )
                    else:
                        socketio.run(
                            app,
                            debug=True,
                            port=8000,
                            host="0.0.0.0",
                            use_reloader=True,
                        )
