# -*- coding: utf-8 -*-
from collections import Sequence, Mapping

from dictlets.plugins.input_preprocessors import DictletInputPreprocessor
from frutils.jinja2_filters import ALL_FILTERS


class FilterPreprocessor(DictletInputPreprocessor):
    def __init__(self, filter_name, filter_args=None, key_names=None, value_names=None):

        self._filter_name = filter_name
        self._filter = ALL_FILTERS.get(filter_name, None)
        if self._filter is None:
            raise ValueError(
                "Can't create FilterPreprocessor, no filter named: {}".format(
                    filter_name
                )
            )
        self._filter = self._filter["func"]
        if filter_args is None:
            filter_args = {}
        self._filter_args = filter_args
        self._key_names = key_names
        self._value_names = value_names

    def preprocess(self, input):

        if not self._key_names and not self._value_names:

            result = self._filter(input, **self._filter_args)
            return result

        if not isinstance(input, Mapping):
            raise TypeError(
                "Input must be of type 'mapping' to be able to apply filter preprocessor: {}".format(
                    input
                )
            )

        if self._key_names:
            raise NotImplementedError("Filtering key names not implemented yet")

        if self._value_names:

            if not isinstance(self._value_names, Sequence):
                value_names = [self._value_names]
            else:
                value_names = self._value_names

            for val in value_names:
                input[val] = self._filter(input[val], **self._filter_args)

            return input
