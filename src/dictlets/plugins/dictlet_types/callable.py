# -*- coding: utf-8 -*-
from typing import Union, Dict, Set

from dictlets.dictlet import Dictlet, DictletMetadata
from dictlets.plugins.input_preprocessors import DictletInputPreprocessor
from dictlets.plugins.multipliers import DictletMultiplier
from dictlets.plugins.result_postprocessors import DictletResultPostprocessor
from dictlets.utils import get_required_property_paths


class CallableDictlet(Dictlet):
    """*Dictlet* that holds a function reference which will be executed when 'retrieve' is called.

      - *callable*: the function to execute (no arguments permitted)

    """

    def __init__(
        self,
        callable,
        id: str = None,
        target: str = None,
        input=None,
        multiplier: Union[Dict, DictletMultiplier] = None,
        preprocess: Union[Dict, DictletInputPreprocessor] = None,
        postprocess: Union[Dict, DictletResultPostprocessor] = None,
        meta: Union[Dict, DictletMetadata] = None,
    ):

        self._callable = callable
        self._current_context = None

        super(CallableDictlet, self).__init__(
            id=id,
            target=target,
            input=input,
            multiplier=multiplier,
            preprocess=preprocess,
            postprocess=postprocess,
            meta=meta,
        )

    def _retrieve(self, input):

        return self._callable(input)

    def _get_input_args(self) -> Set:

        paths = get_required_property_paths(self.input)

        if self.multiplier:
            to_remove = set()
            for path in paths:
                if path.startswith(self.multiplier.alias + "."):
                    to_remove.add(path)

            for rem in to_remove:
                paths.remove(rem)

            multi_paths = self.multiplier.get_input_args()
            paths.update(multi_paths)

        return paths

    # def assemble_tasks(self, input_dictlets):
    #
    #     state = merge_dictlet_results(input_dictlets)
    #     if self.multiplier:
    #         tasks = []
    #         states = self.multiplier.multiply(state)
    #         for extra_target, st in states.items():
    #
    #             task = copy.deepcopy(self)
    #             if not self.target:
    #                 target = extra_target
    #             else:
    #                 target = self.target + "." + extra_target
    #             task.target = target
    #             task._current_context = st
    #             tasks.append(task)
    #
    #         return tasks
    #     else:
    #
    #         task = copy.deepcopy(self)
    #         task._current_context = state
    #         return [task]

    def _get_dictlet_type_name(self) -> str:

        return "callable"
