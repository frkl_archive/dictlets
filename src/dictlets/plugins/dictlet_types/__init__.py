# -*- coding: utf-8 -*-
import copy
import logging
import sys
from typing import Dict, Type

from stevedore import ExtensionManager

from dictlets.dictlet import log


def load_dictlet_types() -> Dict[str, Type]:
    """Loading all dictlet sub-classes and their names.

    The result is a dict containing the dictlet name as key and its type as value.
    """

    log2 = logging.getLogger("stevedore")
    out_hdlr = logging.StreamHandler(sys.stderr)
    out_hdlr.setFormatter(logging.Formatter("dictlets load error -> %(message)s"))
    out_hdlr.setLevel(logging.DEBUG)
    log2.addHandler(out_hdlr)
    log2.setLevel(logging.INFO)

    log.debug("Loading dictlet subclass...")

    mgr = ExtensionManager(
        namespace="dictlets.types", invoke_on_load=False, propagate_map_exceptions=True
    )

    result = {}
    for t in mgr:
        result[t.name] = t.plugin
    return result


def dictlet_from_dict(data):

    data = copy.deepcopy(data)
    dictlet_type = data.pop("type", None)
    if dictlet_type is None:
        raise ValueError("Can't create Dictlet, no 'type' provided: {}".format(data))

    d_type = load_dictlet_types().get(dictlet_type, None)
    if d_type is None:
        raise ValueError(
            "Can't create Dictlet, type '{}' not registered.".format(dictlet_type)
        )
    dictlet = d_type(**data)
    return dictlet
