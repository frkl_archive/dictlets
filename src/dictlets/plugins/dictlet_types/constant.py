# -*- coding: utf-8 -*-
from typing import Set, Union, Dict

import dpath

from dictlets.dictlet import Dictlet, DictletMetadata


class ConstantDictlet(Dictlet):
    """*Dictlet* that holds a static/constant value which represents the result.

      - *value*: the result object
      - *target*: the target path where the retrieved result of this *Dictlet* will be inserted in the overall
            *DictletAssembly* result
      - *provides*: the (optional) schema of the retrieved result value

    """

    def __init__(
        self,
        id: str = None,
        target: str = None,
        input=None,
        meta: Union[Dict, DictletMetadata] = None,
        **args
    ):

        if not input:
            raise ValueError("Input can't be empty for constant dictlet.")

        super(ConstantDictlet, self).__init__(
            id=id, target=target, input=input, meta=meta
        )
        self.update_value(self.input)

    def invalidate(self):

        pass

    def update_value(self, results):

        value = results

        if not self._target:
            self._value_cache = value
        else:
            self._value_cache = {}
            dpath.util.new(self._value_cache, self._target, value, separator=".")

    @property
    def is_retrieved(self):
        return True

    def _retrieve(self, input):
        return self.input

    def _get_input_args(self) -> Set:

        return None

    def _get_dictlet_type_name(self) -> str:

        return "const"

    # def assemble_tasks(self, input_dictlets):
    #
    #     return [self]
