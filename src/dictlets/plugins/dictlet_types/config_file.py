# -*- coding: utf-8 -*-
import os
from pathlib import Path
from typing import Set

from dictlets.dictlet import Dictlet
from dictlets.utils import get_required_property_paths
from frutils import auto_parse_string


class ConfigFileDictlet(Dictlet):
    """*Dictlet* that reads a config file.
    """

    def __init__(self, path, **dictlet_args):

        meta = dictlet_args.get("meta", None)
        if meta is None:
            meta = {}
            dictlet_args["meta"] = meta
        meta["bound_to_local"] = True

        if "input" in dictlet_args.keys():
            raise ValueError(
                "Dictlet config for 'config_file' dictlet can't contain 'input' key: {}".format(
                    dictlet_args
                )
            )

        dictlet_args["input"] = path

        super(ConfigFileDictlet, self).__init__(**dictlet_args)

    def _retrieve(self, input):

        path = Path(os.path.expanduser(input))

        content = path.read_text()
        config = auto_parse_string(content=content, default_if_empty={})
        return config

    def _get_input_args(self) -> Set:

        return get_required_property_paths(self.input)

    def _get_dictlet_type_name(self) -> str:

        return "config_file"
