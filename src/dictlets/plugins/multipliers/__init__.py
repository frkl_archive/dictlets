# -*- coding: utf-8 -*-
import abc
import copy
import logging
import sys
from typing import Dict, Type

import six
from stevedore import ExtensionManager

log = logging.getLogger("dictlets")


def load_dictlet_multipliers() -> Dict[str, Type]:
    """Loading all dictlet multipliers and their aliases.

    The result is a dict containing the multiplier alias as key and its type as value.
    """

    log2 = logging.getLogger("stevedore")
    out_hdlr = logging.StreamHandler(sys.stderr)
    out_hdlr.setFormatter(logging.Formatter("dictlets load error -> %(message)s"))
    out_hdlr.setLevel(logging.DEBUG)
    log2.addHandler(out_hdlr)
    log2.setLevel(logging.INFO)

    log.debug("Loading dictlet subclass...")

    mgr = ExtensionManager(
        namespace="dictlets.multipliers",
        invoke_on_load=False,
        propagate_map_exceptions=True,
    )

    result = {}
    for t in mgr:
        result[t.name] = t.plugin
    return result


def multiplier_from_dict(data):

    data = copy.deepcopy(data)

    alias = data.pop("alias", None)
    if alias is None:
        raise ValueError("Multiplier config needs 'alias' key: {}".format(data))
    m_type = data.pop("type", None)
    if m_type is None:
        raise ValueError("Multiplier config needs 'type' key: {}".format(data))

    multiplier = load_dictlet_multipliers().get(m_type, None)
    if multiplier is None:
        raise ValueError("No multiplier with type '{}' available.".format(m_type))

    return multiplier(alias=alias, **data)


@six.add_metaclass(abc.ABCMeta)
class DictletMultiplier(object):
    def __init__(self, alias):
        self._alias = alias

    @property
    def alias(self):
        return self._alias

    @abc.abstractmethod
    def get_input_args(self):
        pass

    @abc.abstractmethod
    def multiply(self, state):
        pass
