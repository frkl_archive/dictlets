# -*- coding: utf-8 -*-
import itertools
from collections import Mapping, Sequence

from dictlets.plugins.multipliers import DictletMultiplier
from dictlets.utils import get_required_property_paths, replace_queries_in_obj
from frutils import dict_merge


class EachListItemMultiplier(DictletMultiplier):
    def __init__(self, alias, repl_map):

        super(EachListItemMultiplier, self).__init__(alias=alias)
        self._repl_map = repl_map

    def get_input_args(self):
        paths = get_required_property_paths(self._repl_map)
        return paths

    def multiply(self, state):

        new_repl_map = replace_queries_in_obj(self._repl_map, state)

        if not isinstance(new_repl_map, Mapping):
            raise TypeError(
                "Invalid type for DictletMultiplier result '{}': needs to be a mapping".format(
                    type(new_repl_map)
                )
            )

        repl_dict_targets = []
        repl_dict_inputs = []
        for repl_key, value in new_repl_map.items():

            if not isinstance(value, Sequence):
                raise TypeError(
                    "Invalid type for multiplier template (needs to be sequence): {}/{}".format(
                        value, value
                    )
                )
            repl_dict_targets.append(repl_key)
            repl_dict_inputs.append(value)

        input_product = itertools.product(*repl_dict_inputs)
        input_states = {}
        for input_combination in input_product:

            input_state = {}
            extra_target = []
            for index, target_item in enumerate(repl_dict_targets):

                input_item = input_combination[index]
                # TODO: check if hashable as it's used as a dict key
                input_state.setdefault(self.alias, {})[target_item] = input_item
                extra_target.append(input_item)

            input_states[".".join(extra_target)] = dict_merge(
                state, input_state, copy_dct=True
            )

        return input_states
