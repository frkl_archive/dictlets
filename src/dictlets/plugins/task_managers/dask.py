# -*- coding: utf-8 -*-
import copy
import os

from colorama import Style
from distributed import LocalCluster

from dictlets.defaults import dictlets_app_dirs

os.environ["PREFECT__LOGGING__LEVEL"] = "ERROR"  # noqa

from collections import Sequence
from typing import Mapping, Dict, List

from prefect import Flow, Task
from prefect.engine.executors import DaskExecutor
from prefect.utilities.debug import raise_on_exception

from dictlets.plugins.task_managers import TaskManager, DictletTask
from frutils import flatten_lists


def task_state_handler(obj, old_state, new_state):

    silent = True
    if silent:
        return
    import click

    name = obj.name
    if not hasattr(new_state, "map_states"):
        name = "{} subtask".format(name)

    click.echo(
        "{}{}{}: {}".format(Style.BRIGHT, name, Style.RESET_ALL, new_state.message)
    )

    return new_state


class TaskSplitter(Task):
    def __init__(self, name, **kwargs):
        super(TaskSplitter, self).__init__(name=name, **kwargs)

    def run(self, dictlet, finished_tasks=None):

        if finished_tasks is None:
            finished_tasks = []

        if isinstance(finished_tasks, Mapping):
            finished_tasks = [finished_tasks]

        if not isinstance(finished_tasks, Sequence):
            raise TypeError(
                "Invalid input type for task '{}': {}".format(self._dictlet.id),
                type(finished_tasks),
            )

        finished_tasks = flatten_lists(finished_tasks)

        tasks = dictlet.assemble_tasks(
            finished_tasks
        )  # this returns a list of dictlets

        return tasks


class TaskWrapper(Task):
    def __init__(self, name, **kwargs):
        super(TaskWrapper, self).__init__(name=name, **kwargs)

    def run(self, dictlet):

        dictlet.retrieve()
        return dictlet


class DaskTaskManager(TaskManager):
    def __init__(
        self,
        dask_address=None,
        local_processes=True,
        debug=False,
        dask_local_directory=None,
        **kwargs
    ):

        super(DaskTaskManager, self).__init__()

        if dask_address is None:

            if dask_local_directory is None:
                dask_local_directory = os.path.join(
                    dictlets_app_dirs.user_cache_dir, "dask-worker-space"
                )
                os.makedirs(dask_local_directory, exist_ok=True)

            self._cluster = LocalCluster(
                processes=False, local_directory=dask_local_directory, n_workers=4
            )
            dask_address = self._cluster.scheduler_address

        self._dask_address = dask_address
        self._debug = debug
        self._local_processes = local_processes
        self._kwargs = kwargs

        # self._dask_client = None

        self._executor = DaskExecutor(
            address=self._dask_address,
            local_processes=self._local_processes,
            debug=self._debug,
            **self._kwargs
        )

    # @property
    # def dask_client(self) -> Client:
    #
    #     if self._dask_client is not None:
    #         return self._dask_client
    #
    #     if self._debug:
    #         lg = logging.WARNING
    #     else:
    #         lg = logging.CRITICAL
    #
    #     self._dask_client = Client(self._dask_address, processes=self._local_processes, silence_logs=lg, **self._kwargs)
    #     return self._dask_client

    def execute_dictlet_task(self, task: DictletTask) -> List[Dict]:

        dictlet_map = task.dictlet_map
        flow = self.create_flow(dictlet_map)
        # executor = LocalExecutor()

        # flow.visualize()

        with raise_on_exception():

            state = flow.run(executor=self._executor)

        results = []
        for k, res in state.result.items():

            if not isinstance(k, TaskWrapper):
                continue

            for val in res._result.value:
                results.append(val)

        return results

    def create_flow(self, dictlet_map):

        flow = Flow("test")
        tasks = {}

        for dictlet, deps in dictlet_map.items():
            dictlet = copy.deepcopy(dictlet)  # we don't need the callbacks
            task = TaskWrapper(dictlet.id, state_handlers=[task_state_handler])
            splitter = TaskSplitter(
                name="splitter_" + dictlet.id, state_handlers=[task_state_handler]
            )
            multi = task.map(flow=flow, dictlet=splitter)
            tasks[dictlet.id] = {
                "deps": deps,
                "dictlet": dictlet,
                "splitter": splitter,
                "multi": multi,
            }
            flow.add_task(splitter)

        for dictlet_id, details in tasks.items():
            deps = []
            for dep in details["deps"]:
                dep_task = tasks[dep]
                deps.append(dep_task["multi"])

            splitter = details["splitter"]
            splitter.bind(flow=flow, dictlet=details["dictlet"], finished_tasks=deps)
            multi = details["multi"]
            flow.add_task(multi)

        return flow
