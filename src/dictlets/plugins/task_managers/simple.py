# -*- coding: utf-8 -*-
import copy
from typing import List, Dict

from dictlets.plugins.task_managers import TaskManager, DictletTask


class SimpleTaskManager(TaskManager):
    def __init__(self):

        super(SimpleTaskManager, self).__init__()

    def execute_dictlet_task(self, task: DictletTask) -> List[Dict]:

        dictlet_map = task.dictlet_map
        do = self.create_dependency_order(dictlet_map)

        finished_tasks = []
        for run in do:
            current_run_finished = []
            for dictlet in run:

                dictlet = copy.deepcopy(dictlet)
                tasks = dictlet.assemble_tasks(finished_tasks)
                for t in tasks:
                    t.retrieve()
                    current_run_finished.append(t)

            finished_tasks.extend(current_run_finished)

        return finished_tasks

    def create_dependency_order(self, dictlet_map):

        order = []
        used = set()
        used_this_round = set()
        temp = []

        i = 0
        while True:
            i = i + 1

            for dictlet, deps in dictlet_map.items():
                if dictlet.id in used:
                    continue

                all_deps_used = True
                if not deps:
                    temp.append(dictlet)
                    used.add(dictlet.id)
                    used_this_round.add(dictlet.id)
                    continue

                for dep in deps:
                    if dep not in used or dep in used_this_round:
                        all_deps_used = False
                        break

                if not all_deps_used:
                    # can't resolve the dictlet in this round
                    continue
                else:
                    temp.append(dictlet)
                    used.add(dictlet.id)
                    used_this_round.add(dictlet.id)

            if temp:
                order.append(temp)
                temp = []
                used_this_round = set()
            else:
                break

        return order
