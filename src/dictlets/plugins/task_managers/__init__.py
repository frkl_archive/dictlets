# -*- coding: utf-8 -*-
import abc
import copy
import logging
import os
import sys
import threading
import uuid
from collections import Mapping, OrderedDict, Sequence
from datetime import datetime
from typing import Dict, Type, List

import six
from stevedore import ExtensionManager

log = logging.getLogger("dictlet")


def task_manager_load_error(*args):

    if "No module named 'prefect'" == (str(args[2])):
        return

    raise args[2]


def load_task_managers() -> Dict[str, Type]:
    """Loading all dictlet multipliers and their aliases.

    The result is a dict containing the multiplier alias as key and its type as value.
    """

    log2 = logging.getLogger("stevedore")
    out_hdlr = logging.StreamHandler(sys.stderr)
    out_hdlr.setFormatter(
        logging.Formatter("dictlets task_manager error -> %(message)s")
    )
    out_hdlr.setLevel(logging.DEBUG)
    log2.addHandler(out_hdlr)
    log2.setLevel(logging.INFO)

    log.debug("Loading dictlet task_manager...")

    mgr = ExtensionManager(
        namespace="dictlets.task_managers",
        invoke_on_load=False,
        propagate_map_exceptions=True,
        on_load_failure_callback=task_manager_load_error,
    )

    result = {}
    for t in mgr:
        result[t.name] = t.plugin

    return result


def get_task_manager(data):

    if issubclass(data.__class__, TaskManager):
        return data

    if data:
        data = copy.deepcopy(data)
    else:
        data = {}

    if not data:
        from dictlets.plugins.task_managers.simple import SimpleTaskManager

        task_manager = SimpleTaskManager()
        return task_manager
    elif isinstance(data, six.string_types):
        data = {"type": data}

    if not isinstance(data, Mapping):
        raise TypeError(
            "Can't create task manager object from type: {}".format(type(data))
        )

    m_type = data.pop("type", "auto")

    if m_type == "auto":
        try:
            os.environ["PREFECT__LOGGING__LEVEL"] = "ERROR"  # noqa
            import prefect  # noqa

            m_type = "prefect"
        except (Exception):
            m_type = "simple"
        log.debug("Auto selected task manager: {}".format(m_type))

    task_manager = load_task_managers().get(m_type, None)
    if task_manager is None:
        raise ValueError(
            "No dictlet task_manager with type '{}' available.".format(m_type)
        )

    if data:
        return task_manager(**data)
    else:
        return task_manager()


TASK_STATUS_MAP = {"created": 0, "running": 1, "finished": 2}


class DictletTask(object):
    def __init__(self, dictlet_map: Dict, func, result_callback=None):

        self._id = str(uuid.uuid4())
        self._dictlet_map = dictlet_map
        self._func = func
        self._result = None
        self._success = None
        self._error = None
        self._timestamps = {}
        self.status = "created"
        self._result_available = threading.Event()
        if result_callback is None:
            result_callback = []
        if not isinstance(result_callback, Sequence):
            result_callback = [result_callback]
        self._result_callbacks = result_callback

    @property
    def id(self):
        return self._id

    @property
    def status_string(self):
        return next(
            key for key, value in TASK_STATUS_MAP.items() if value == self.status
        )

    @property
    def status(self):
        return self._status

    def __eq__(self, other):

        if not isinstance(other, DictletTask):
            return False
        return self.id == other.id

    def __hash__(self):

        return hash(self.id)

    @status.setter
    def status(self, status):
        if isinstance(status, six.string_types):
            status = TASK_STATUS_MAP[status]

        self._status = status
        self._timestamps[status] = datetime.now()

    @property
    def dictlet_map(self):
        return self._dictlet_map

    @property
    def dictlet_ids(self):

        return [d.id for d in self._dictlet_map.keys()]

    def run(self):
        self.status = "running"
        try:
            result = self._func(self)
            self._result = result
            self._success = True
            # self._dictlet_set.set_results(self._result)
        except (Exception) as e:
            self._success = False
            self._error = e
            result = None

        self.status = "finished"
        if self._result_callbacks:
            for rc in self._result_callbacks:
                rc(self)
        self._result_available.set()
        return result

    @property
    def result(self):

        if self.status < TASK_STATUS_MAP["finished"]:
            return None

        return self._result

    def wait(self, timeout=None):

        self._result_available.wait(timeout=timeout)


@six.add_metaclass(abc.ABCMeta)
class TaskManager(object):
    def __init__(self):

        self._tasks = OrderedDict()
        self._archived_tasks = OrderedDict()

        # self._result_callbacks = [self._task_finished]
        self._result_callbacks = []

    def add_result_callback(self, result_callback):

        self._result_callbacks.append(result_callback)

    @property
    def info(self):
        return self._get_info()

    def _get_info(self):

        return {"class": self.__class__.__name__}

    # def _task_finished(self, task):
    #
    #     print("Task finished: {}".format(task.id))

    def get_task(self, task_id):

        if task_id not in self._tasks.keys():
            raise ValueError("No task with id '{}' available.".format(task_id))

        return self._tasks[task_id]

    @property
    def tasks(self):
        return self._tasks

    def get_tasks(self, min_status=None, max_status=None):

        if min_status is None:
            min_status = -1
        if max_status is None:
            max_status = 99999

        if isinstance(min_status, six.string_types):
            min_status = TASK_STATUS_MAP[min_status]
        if isinstance(max_status, six.string_types):
            max_status = TASK_STATUS_MAP[max_status]

        result = []
        for t in self.tasks.values():
            if t.status >= min_status and t.status <= max_status:
                result.append(t)

        return result

    def retrieve_dictlet_values(self, dictlet_map) -> DictletTask:

        task = DictletTask(
            dictlet_map=dictlet_map,
            func=self.execute_dictlet_task,
            result_callback=self._result_callbacks,
        )
        self.submit(task)

        return task

    def submit(self, task):

        self._tasks[task.id] = task

        bg_thread = threading.Thread(target=task.run)
        bg_thread.start()
        # self.bg_func(task.run)
        return bg_thread

    @abc.abstractmethod
    def execute_dictlet_task(self, task) -> List[Dict]:
        """Execute the dictlet task to retrieve the values.

        This happens typically in a background thread.
        """
        pass
