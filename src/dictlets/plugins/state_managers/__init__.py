# -*- coding: utf-8 -*-
import abc
import copy
import logging
import os
import sys
from typing import Dict, Type

import six
from stevedore import ExtensionManager

from dictlets.plugins.dictlet_callbacks import DictletCallback
from dictlets.plugins.dictlet_types import dictlet_from_dict
from dictlets.utils import check_for_duplicates

log = logging.getLogger("dictlets")


def load_result_state_managers() -> Dict[str, Type]:
    """Loading all dictlet multipliers and their aliases.

    The result is a dict containing the multiplier alias as key and its type as value.
    """

    log2 = logging.getLogger("stevedore")
    out_hdlr = logging.StreamHandler(sys.stderr)
    out_hdlr.setFormatter(
        logging.Formatter("dictlet state manager load error -> %(message)s")
    )
    out_hdlr.setLevel(logging.DEBUG)
    log2.addHandler(out_hdlr)
    log2.setLevel(logging.INFO)

    log.debug("Loading dictlet state manager subclasses...")

    mgr = ExtensionManager(
        namespace="dictlets.state_managers",
        invoke_on_load=False,
        propagate_map_exceptions=True,
    )

    result = {}
    for t in mgr:
        result[t.name] = t.plugin
    return result


def get_state_manager(data):

    if issubclass(data.__class__, DictletStateManager):
        return data

    if not data:
        data = {"type": "folder", "path": os.path.join(os.getcwd(), ".dictlet_project")}

    data = copy.deepcopy(data)

    m_type = data.pop("type", None)
    if m_type is None:
        raise ValueError("State manager config needs 'type' key: {}".format(data))

    state_manager = load_result_state_managers().get(m_type, None)
    if state_manager is None:
        raise ValueError("No state manager with type '{}' available.".format(m_type))

    return state_manager(**data)


@six.add_metaclass(abc.ABCMeta)
class DictletStateManager(DictletCallback):
    def __init__(self):

        self._dictlets = {}

    @property
    def dictlets(self):
        return self._dictlets

    @property
    def info(self):
        return self._get_info()

    def add_dictlet(self, dictlet_config, dictlet_state=None):

        dictlet_config = copy.deepcopy(dictlet_config)
        # if dictlet_config.get("id", None):
        #     dictlet_config["id"] = generate_random_id(prefix=dictlet_config["type"])

        dictlet = self.load_dictlet(
            dictlet_config=dictlet_config, dictlet_state=dictlet_state
        )

        dictlet_config["id"] = dictlet.id
        self.store_dictlet(dictlet_config)

        return dictlet

    def load_dictlet(self, dictlet_config, dictlet_state=None):

        dictlet = self._create_dictlet(
            dictlet_config=dictlet_config, dictlet_state=dictlet_state
        )

        check_for_duplicates(self.dictlets.values(), dictlet)

        self._dictlets[dictlet.id] = dictlet
        dictlet.add_callback(self)
        return dictlet

    def _create_dictlet(self, dictlet_config, dictlet_state=None):

        dictlet = dictlet_from_dict(dictlet_config)

        if dictlet_state is not None:
            dictlet._is_retrieved = True
            dictlet._value_cache = dictlet_state["value"]

        return dictlet

    def dictlet_event(self, dictlet, event_type, old_state=None, new_state=None):
        log.debug(("Event: {} - dictlet: {}".format(event_type, dictlet.id)))
        if event_type == "value_updated":
            self.write_dictlet_state(dictlet, new_state)
        elif event_type == "dictlet_invalidated":
            self.invalidate_dictlet(dictlet)

    def _get_info(self):

        return {"class": self.__class__.__name__}

    @abc.abstractmethod
    def init(self):
        """Initialize state for a new project."""
        pass

    @abc.abstractmethod
    def is_initialized(self):
        """Return whether dictlet state is initialized or not."""
        pass

    @abc.abstractmethod
    def write_dictlet_state(self, dictlet, state):
        """Set a dictlet state."""
        pass

    @abc.abstractmethod
    def invalidate_dictlet(self, dictlet):
        """Invalidate a dictlet (delete state)."""
        pass

    @abc.abstractmethod
    def store_dictlet(self, dictlet_config):
        """Add dictlet to store."""

        pass

    @abc.abstractmethod
    def load_dictlets(self):
        """Load all dictlets from the store."""

        pass

    def __eq__(self, other):

        if not issubclass(other.__class__, DictletStateManager):
            return False

    def __hash__(self):

        return hash(self._unique_attr())

    @abc.abstractmethod
    def _unique_attr(self):
        """Return a unique attribute.

        This is used to check whether a state manager is already in use. For example, in the case of the
        FolderStateManager, it would return an absolute path.
        """

        pass

    def destroy(self):

        self._destroy()
        self._dictlets = {}

    @abc.abstractmethod
    def _destroy(self):
        """Delete all dictlets and state."""

        pass
