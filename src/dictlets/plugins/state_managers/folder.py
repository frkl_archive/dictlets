# -*- coding: utf-8 -*-
import json
import logging
import os
import shutil
import threading
from pathlib import Path

import fasteners
from ruamel.yaml.comments import CommentedMap
from six import string_types

from dictlets.plugins.state_managers import DictletStateManager
from frutils import readable

log = logging.getLogger("dictlets")


class FolderStateManager(DictletStateManager):
    def __init__(self, path: Path):

        super(FolderStateManager, self).__init__()

        self._lock = threading.Lock()

        if isinstance(path, string_types):
            path = Path(os.path.expanduser(path))
        self._path = path
        if self._path.exists():
            if not self._path.is_dir():
                raise Exception(
                    "State path not a folder: {}".format(self._path.as_posix())
                )

        self._dictlet_path = self._path / "dictlets"
        if self._dictlet_path.exists():
            if not self._dictlet_path.is_dir():
                raise Exception(
                    "Dictlet state path not a folder: {}".format(
                        self._dictlet_path.as_posix()
                    )
                )

        super(FolderStateManager, self).__init__()

    def init(self):

        self._path.mkdir(parents=True)
        self._dictlet_path.mkdir(parents=True)

    def is_initialized(self):
        return self._dictlet_path.is_dir()

    def load_dictlets(self):

        if not self._dictlet_path.exists():
            return

        all = []
        for f in self._dictlet_path.iterdir():
            if not f.is_dir():
                continue

            dictlet_config_path = f / "dictlet.config"

            if not dictlet_config_path.exists():
                raise Exception(
                    "Can't load dictlet from path, no 'dictlet.config' file present: {}".format(
                        dictlet_config_path
                    )
                )
            config = json.loads(dictlet_config_path.read_text())

            if config["id"] != f.name:
                raise Exception(
                    "Invalid dictlet config, id differs between 'dictlet.config' and folder name: {}".format(
                        f
                    )
                )

            dictlet_state = f / "dictlet.state"
            if dictlet_state.exists():
                content = dictlet_state.read_text()
                state = json.loads(content)
            else:
                state = None

            all.append((config, state))

        for d, s in all:
            self.load_dictlet(dictlet_config=d, dictlet_state=s)

    def store_dictlet(self, dictlet_config):

        dictlet_path = self._dictlet_path / dictlet_config["id"]
        dictlet_path.mkdir(parents=True, exist_ok=True)
        dictlet_config_path = dictlet_path / "dictlet.config"

        if dictlet_config_path.exists():
            raise Exception(
                "Can't store new dictlet '{}', path alraedy exists: {}".format(
                    dictlet_config["id"], dictlet_config_path
                )
            )

        dictlet_config_path.write_text(readable(dictlet_config, out="json", indent=2))
        return

    def invalidate_dictlet(self, dictlet):

        dictlet_path = self._dictlet_path / dictlet.id
        dictlet_state_path = dictlet_path / "dictlet.state"

        if dictlet_state_path.exists():
            dictlet_state_path.unlink()

    @fasteners.locked
    def write_dictlet_state(self, dictlet, state):

        dictlet_path = self._dictlet_path / dictlet.id
        dictlet_state_path = dictlet_path / "dictlet.state"

        dictlet_state_path.write_text(readable(state, out="json", indent=2))

    def _destroy(self):

        shutil.rmtree(self._path)

    def _unique_attr(self):

        return self._path

    def _get_info(self):

        info = CommentedMap()
        info["class"] = self.__class__.__name__
        info["state_path"] = self._path.as_posix()
        info["dictlet_path"] = self._dictlet_path.as_posix()

        return info
