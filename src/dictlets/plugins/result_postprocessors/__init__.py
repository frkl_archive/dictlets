# -*- coding: utf-8 -*-
import abc
import copy
import logging
import sys
from typing import Dict, Type

import six
from stevedore import ExtensionManager

log = logging.getLogger("dictlet")


def load_result_postprocessors() -> Dict[str, Type]:
    """Loading all dictlet multipliers and their aliases.

    The result is a dict containing the multiplier alias as key and its type as value.
    """

    log2 = logging.getLogger("stevedore")
    out_hdlr = logging.StreamHandler(sys.stderr)
    out_hdlr.setFormatter(
        logging.Formatter("dictlet result postprocessors load error -> %(message)s")
    )
    out_hdlr.setLevel(logging.DEBUG)
    log2.addHandler(out_hdlr)
    log2.setLevel(logging.INFO)

    log.debug("Loading dictlet result postprocessor subclasses...")

    mgr = ExtensionManager(
        namespace="dictlets.result_postprocessors",
        invoke_on_load=False,
        propagate_map_exceptions=True,
    )

    result = {}
    for t in mgr:
        result[t.name] = t.plugin

    return result


def result_postprocessor_from_dict(data):

    data = copy.deepcopy(data)

    m_type = data.pop("type", None)
    if m_type is None:
        raise ValueError("Input preprocessor config needs 'type' key: {}".format(data))

    postproc = load_result_postprocessors().get(m_type, None)
    if postproc is None:
        raise ValueError(
            "No result postprocessor with type '{}' available.".format(m_type)
        )

    return postproc(**data)


@six.add_metaclass(abc.ABCMeta)
class DictletResultPostprocessor(object):
    @abc.abstractmethod
    def postprocess(self, result):

        pass
