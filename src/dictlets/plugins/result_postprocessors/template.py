# -*- coding: utf-8 -*-

from dictlets.defaults import DEFAULT_DICTLETS_JINJA_ENV
from dictlets.plugins.result_postprocessors import DictletResultPostprocessor
from frutils import replace_strings_in_obj, dict_merge


class TemplateResultPostprocessor(DictletResultPostprocessor):
    def __init__(self, template_obj):

        self._template_obj = template_obj

    def postprocess(self, input, result, state):

        # TODO: validate input
        repl = dict_merge(
            state,
            {"__result__": result, "__input__": input, "__state__": state},
            copy_dct=True,
        )

        result = replace_strings_in_obj(
            self._template_obj,
            replacement_dict=repl,
            jinja_env=DEFAULT_DICTLETS_JINJA_ENV,
        )
        return result
