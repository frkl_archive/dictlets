# -*- coding: utf-8 -*-
import abc
import copy
import logging
import sys
import uuid
from collections import Mapping
from typing import Dict, Type

import six
from stevedore import ExtensionManager

log = logging.getLogger("dictlets")


def load_dictlet_callback() -> Dict[str, Type]:
    """Loading all dictlet multipliers and their aliases.

    The result is a dict containing the multiplier alias as key and its type as value.
    """

    log2 = logging.getLogger("stevedore")
    out_hdlr = logging.StreamHandler(sys.stderr)
    out_hdlr.setFormatter(logging.Formatter("dictlet callback error -> %(message)s"))
    out_hdlr.setLevel(logging.DEBUG)
    log2.addHandler(out_hdlr)
    log2.setLevel(logging.INFO)

    log.debug("Loading dictlet callback...")

    mgr = ExtensionManager(
        namespace="dictlets.callbacks",
        invoke_on_load=False,
        propagate_map_exceptions=True,
    )

    result = {}
    for t in mgr:
        result[t.name] = t.plugin
    return result


def dictlet_callback_from_dict(data):

    data = copy.deepcopy(data)

    m_type = data.pop("type", None)
    if m_type is None:
        raise ValueError("Dictlet callback config needs 'type' key: {}".format(data))

    callback = load_dictlet_callback().get(m_type, None)
    if callback is None:
        raise ValueError("No dictlet callback with type '{}' available.".format(m_type))

    if data:
        return callback(**data)
    else:
        return callback()


def create_callback(callback):

    if issubclass(callback.__class__, DictletCallback):
        return callback

    if isinstance(callback, six.string_types):
        callback = {"type": callback}

    if not isinstance(callback, Mapping):
        raise TypeError(
            "Invalid type for callback (needs string, mapping, or DictletCallback): {}".format(
                type(callback)
            )
        )

    callback = dictlet_callback_from_dict(callback)
    return callback


@six.add_metaclass(abc.ABCMeta)
class DictletCallback(object):
    def __init__(self, id=None):

        if id is None:
            id = str(uuid.uuid4())
        self._id = id

    def callback_id(self):
        return self._id

    @abc.abstractmethod
    def dictlet_event(self, dictlet, event_type, old_state=None, new_state=None):

        pass

    def __eq__(self, other):

        if self.__class__ != other.__class__:
            return False

        return self.callback_id == other.callback_id

    def __hash__(self):
        return hash(self._get_callback_id())
