# -*- coding: utf-8 -*-

from dictlets.plugins.dictlet_callbacks import DictletCallback
from frutils.frutils_cli import output_to_terminal


class TerminalOutputCallback(DictletCallback):
    def __init__(self, id=None):

        super(TerminalOutputCallback, self).__init__(id=id)

    def dictlet_event(self, dictlet, event_type, old_state=None, new_state=None):

        msg = "{}: {}".format(dictlet.alias, event_type)
        output_to_terminal(msg)
