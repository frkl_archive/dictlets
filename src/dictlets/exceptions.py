# -*- coding: utf-8 -*-
from frutils.exceptions import FrklException


class DictletValueException(Exception):
    def __init__(self, dictlet_value, msg):

        self._dictlet_value = dictlet_value
        super(DictletValueException, self).__init__(msg)


class DictletPostprocessException(FrklException):
    def __init__(
        self,
        dictlet,
        postprocessor,
        msg=None,
        solution=None,
        references=None,
        reason=None,
        parent=None,
    ):

        self._dictlet = dictlet
        self._postprocessor = postprocessor
        super(DictletPostprocessException, self).__init__(
            msg=msg,
            reason=reason,
            solution=solution,
            references=references,
            parent=parent,
        )
