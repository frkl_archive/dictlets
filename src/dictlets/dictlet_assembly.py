# -*- coding: utf-8 -*-
import threading
from collections import Sequence
from typing import List, Dict, Any, Mapping, Set, Tuple

import dpath
import fasteners
import six
from jinja2schema.model import Dictionary

from dictlets.defaults import DEFAULT_DICTLETS_JINJA_ENV
from dictlets.dictlet import Dictlet
from dictlets.plugins.dictlet_callbacks import create_callback
from dictlets.plugins.dictlet_types.constant import ConstantDictlet
from dictlets.utils import (
    query_is_glob,
    get_template_schema,
    check_for_duplicates,
    query_dict,
)
from frutils import string_is_templated
from frutils.frutils import (
    get_leaves_from_dict,
    readable,
    dict_merge,
    get_template_keys,
)


class DictletAssembly(Dictlet):
    """A *Dictlet* class that contains other *Dictlet*s.

    A single *Dictlet* is an atomic building block to create a *AssemblyStructure* (via the *DictletsDictlet* class).
     Each *Dictlet* holds a value, which will be inserted in the structure according to a *Dictlet* target property.
     The value is resolved indirectly, in order to disconnect potential computations from the class itself, and enable
     use of distributed and/or parallel execution.

    - *aid*: the id for this dictlet, used for lookup purposes in a dictlet *AssemblyStructure*
    - *target*: the target path of this Dictletlets value in case it is a child of another *DictletAssembly*
    - *input*: any potential input that is necessary to resolve a value
    - *dictlets*: a list of *Dictlet*s to seed this object with
    - *callbacks*: a list of callbacks that get informed on certain internal events
    """

    def __init__(
        self,
        id: str = None,
        target: str = None,
        input=None,
        dictlets=None,
        callbacks=None,
        meta=None,
    ):

        self._structure = None
        self._values = None
        self._result = None
        self._current_state = {}

        super(DictletAssembly, self).__init__(
            id=id, target=target, input=input, callbacks=callbacks, meta=meta
        )

        self._lock = threading.Lock()

        if dictlets is None:
            dictlets = []
        if not isinstance(dictlets, Sequence):
            raise TypeError(
                "'dictlets' argument needs to be a sequence: {}".format(dictlets)
            )
        self._dictlets = []
        for d in dictlets:
            self.add_dictlet(d)

        self._update_current_state()

    @property
    def dictlets(self):
        """Return the input string/object of this *DictletAssembly*."""

        return self._dictlets

    @property
    def dictlet_ids(self):

        ids = []
        for d in self.dictlets:
            ids.append(d.id)

        return ids

    def _create_dictlet(self, dictlet):

        if dictlet is None:
            raise ValueError("Can't create Dictlet, no value provided.")

        if issubclass(dictlet.__class__, Dictlet):
            return dictlet

        if not isinstance(dictlet, Mapping):
            raise ValueError(
                "Can't create Dictlet, must be either a subclass of the 'Dictlet' class, or a Mapping: {}".format(
                    dictlet
                )
            )

        return ConstantDictlet(input=dictlet)

    def add_dictlet(self, dictlet) -> None:

        dictlet = self._create_dictlet(dictlet)
        if dictlet.parent is not None:
            raise ValueError(
                "Dictlet '{}' already has parent: {}".format(dictlet.id, dictlet.parent)
            )

        dictlet.parent = self.id

        check_for_duplicates(self.dictlets, dictlet)

        for cb in self._callbacks:
            dictlet.add_callback(cb)

        self._dictlets.append(dictlet)
        self.send_event("dictlet_added", new_state=dictlet)
        self._invalidate_structure()

        self._update_current_state()

    def remove_dictlet(self, dictlet_id) -> None:

        self.invalidate_dictlets(dictlet_id)

        dictlet = self.get_dictlet(dictlet_id)
        dictlet.parent = None

        for cb in self._callbacks:
            dictlet.remove_callback(cb)

        self._dictlets.remove(dictlet)
        self.send_event("dictlet_removed", old_state=dictlet)
        self._invalidate_structure()
        self._update_current_state()

    def add_callback(self, callback):

        callback = create_callback(callback)

        for dictlet in self.dictlets:
            dictlet.add_callback(callback)
        self._callbacks.append(callback)
        self.send_event("callback_added", new_state=callback)

    def remove_callback(self, callback):

        callback = create_callback(callback)
        for dictlet in self.dictlets:
            dictlet.remove_callback(dictlet)

        self._callbacks.remove(callback)
        self.send_event("callback_removed", old_state=callback)

    def _invalidate_structure(self):

        old_structure = self._structure
        self._structure = None

        new_structure = self.structure

        self.send_event(
            "structure_invalidated", old_state=old_structure, new_state=new_structure
        )
        self._update_current_state()

    def invalidate_dictlets(self, dictlet_ids=None):

        if dictlet_ids:
            if isinstance(dictlet_ids, six.string_types):
                dictlet_ids = [dictlet_ids]

            ids = set()
            for dictlet_id in dictlet_ids:
                if issubclass(dictlet_id.__class__, Dictlet):
                    dictlet_id = dictlet_id.id
                elif isinstance(dictlet_id, six.string_types):
                    dictlet_id = self.get_dictlet(dictlet_id).id
                ids.add(dictlet_id)

            all_deps = set()
            for id in ids:
                all_deps.add(id)
                deps = self.get_dependencies_for_dictlet(id)
                all_deps.update(deps)

            for dep in all_deps:
                dictlet = self.get_dictlet(dep)
                dictlet.invalidate()
            self.invalidate(invalidate_childs=False)
        else:
            self.invalidate(invalidate_childs=True)

    def invalidate(self, invalidate_childs=False):

        # old_state = self._current_state
        old_value = self._value_cache

        if invalidate_childs:
            for dictlet in self.dictlets:
                dictlet.invalidate()

        self._is_retrieved = False
        self._result = None
        self._value_cache = None
        self._run_meta = None
        self._run_skipped = None

        self.send_event("dictlet_invalidated", old_state=old_value)
        self._update_current_state()

    @property
    def structure(self):
        """Returns a 'shadow' structure of this objects assembled *DictletAssembly* values.
        """

        if self._structure is None:
            self._structure = AssemblyStructure(self.dictlets)

        return self._structure

    def get_dictlet(self, id) -> Dictlet:
        """Return the dictlet matching this id.

        If a full match is found, the corresponding dictlet is returned. If no full match is found, this function
        will check whether exactly one id matches the provided token, if that's the case, that dictlet will be returned.
        If more than one matches are found, an Exception is thrown.

        If no match is found, None will be returned
        """

        for dictlet in self.dictlets:
            if dictlet.id == id:
                return dictlet

        matching_ids = []
        for dictlet in self.dictlets:
            if id in dictlet.id:
                matching_ids.append(dictlet)

        if len(matching_ids) > 1:
            raise ValueError(
                "More than one child dictlet matching id '{}': {}".format(
                    id, matching_ids
                )
            )
        elif len(matching_ids) == 1:
            return matching_ids[0]
        else:
            raise ValueError("No dictlet that matches id: {}".format(id))

    def get_dependencies_for_dictlet(
        self, dictlet_id, current: Set[str] = None
    ) -> Set[str]:
        """Return all dependencies of a specific dictlet id.

        - *dictlet_id*: the id of the dictlet in question
        - *current*: a set of dependencies (specify 'None', this is used for recursive dependency lookup)
        """

        if current is None:
            current = set()

        if dictlet_id in current:
            return current

        dictlet = self.get_dictlet(dictlet_id)

        dep_paths = dictlet.input_args

        if not dep_paths:
            return current

        for dp in dep_paths:

            deps = self.structure.get_dependencies_for_query(dp)

            for d in deps:
                if d != dictlet_id:
                    current.add(d)
                    self.get_dependencies_for_dictlet(d, current=current)

        return current

    def get_dictlet_values(self, dictlet_ids=None, only_retrieved=True):

        if not only_retrieved:
            raise NotImplementedError()

        if not dictlet_ids:
            dictlet_ids = self.dictlet_ids

        result = {}
        for d in dictlet_ids:
            dictlet = self.get_dictlet(d)
            if not dictlet.is_retrieved:
                continue
            result[dictlet.id] = dictlet.value

        return result

    def calculate_dependencies_for_dictlets(self, dictlet_ids) -> Tuple[Dict, bool]:

        if not dictlet_ids:
            dictlet_ids = self.dictlet_ids

        dictlet_map, all_retrieved = self._assemble_dictlet_map(dictlet_ids)
        return dictlet_map, all_retrieved

    def calculate_dependencies_for_query(self, query_string) -> Tuple[Dict, bool]:

        dependency_dictlet_map, all_retrieved = self._assemble_query_dictlet_map(
            query_string=query_string
        )

        return dependency_dictlet_map, all_retrieved

    def query(self, query_string, force=False):
        """Query the structure of this dictlet sat.

        - *query_string*: the query, can include globs ('*')
        - *force*: whether to retrieve all missing dictlet values that are required for this query to execute
        """

        self.send_event("query_started", new_state={"query": query_string})
        dependency_dictlet_map, all_retrieved = self.calculate_dependencies_for_query(
            query_string=query_string
        )

        if not all_retrieved:
            if not force:
                raise Exception(
                    "Not all dictlet values retrieved for query: {}".format(
                        query_string
                    )
                )
            else:
                from dictlets.plugins.task_managers.simple import SimpleTaskManager

                task_manager = SimpleTaskManager()
                task = task_manager.retrieve_dictlet_values(dependency_dictlet_map)
                task.wait()
                self.set_results(task.result)
        query_result = query_dict(
            query_string=query_string, dict_obj=self.current_state
        )

        self.send_event(
            "query_finished", new_state={"query": query_string, "result": query_result}
        )
        return query_result

    def ensure_retrieved(self, dictlet_ids=None, force=False):

        dictlet_map, all_retrieved = self.calculate_dependencies_for_dictlets(
            dictlet_ids=dictlet_ids
        )

        if not all_retrieved:
            if not force:
                raise Exception("Not all dictlet values retrieved.")
            else:
                from dictlets.plugins.task_managers.simple import SimpleTaskManager

                task_manager = SimpleTaskManager()
                task = task_manager.retrieve_dictlet_values(dictlet_map)
                task.wait()
                self.set_results(task.result)
                return True

    @fasteners.locked
    def set_results(self, result_dictlets):

        updated_dictlets = {}
        for dictlet in result_dictlets:

            if dictlet._run_skipped:
                continue
            updated_dictlets.setdefault(dictlet.id, []).append(dictlet._result)

        for dictlet_id, results in updated_dictlets.items():
            local_dictlet = self.get_dictlet(dictlet_id)
            local_dictlet.update_value(results)

        self._update_current_state()

    @property
    def current_state(self):

        return self._current_state

    def _update_current_state(self):

        old_state = self._current_state

        current = {}
        for dictlet in self.dictlets:
            if not dictlet.is_retrieved:
                continue
            dict_merge(current, dictlet.value, copy_dct=False)

        self._current_state = current
        self.send_event(
            event_type="state_changed",
            old_state=old_state,
            new_state=self._current_state,
        )

        return current

    def _assemble_query_dictlet_map(self, query_string):

        root_dependencies = self.structure.get_dependencies_for_query(
            query=query_string
        )

        return self._assemble_dictlet_map(dictlet_ids=root_dependencies)

    def _assemble_dictlet_map(self, dictlet_ids):

        all_retrieved = True

        all_deps = {}
        for rd in dictlet_ids:

            dictlet = self.get_dictlet(rd)

            if not dictlet.is_retrieved:
                all_retrieved = False

            d = self.get_dependencies_for_dictlet(dictlet.id)

            all_deps.setdefault(rd, set()).update(d)

        dependency_dictlets = {}
        dependency_dictlet_ids = set()
        for id, deps in all_deps.items():
            dictlet = self.get_dictlet(id)
            dependency_dictlets[dictlet] = deps
            dependency_dictlet_ids.add(id)

            for dep in deps:
                if dep not in dependency_dictlet_ids:
                    dependency_dictlets[self.get_dictlet(dep)] = set()

            if not dictlet.is_retrieved:
                all_retrieved = False

        return (dependency_dictlets, all_retrieved)

    def _get_dictlet_type_name(self):
        return "dictlet_set"

    def _get_input_args(self) -> Set:

        result = set()

        return result

    def assemble_tasks(self, input_dictlets):

        return [self]

    def _retrieve(self):

        raise NotImplementedError("Can't use DictletAssembly as Dictlet yet.")


class AssemblyStructure(object):
    """A class to describe how a list of *Dictlets* objects assemble a result value structure.

    - *dictlets*: a list of *DictletAssembly* objects
    """

    def __init__(self, dictlets: List[Dictlet]):

        if not dictlets:
            dictlets = []
        self._dictlets = dictlets
        self._dictlet_type_names = []
        self._dictlet_ids = []
        self._dictlet_ids_per_type = {}

        for d in dictlets:
            if d.dictlet_type_name not in self._dictlet_type_names:
                self._dictlet_type_names.append(d.dictlet_type_name)
            self._dictlet_ids.append(d.id)
            self._dictlet_ids_per_type.setdefault(d.dictlet_type_name, []).append(d.id)
        self._structure_value = None

    def to_dict(self) -> Dict:
        """Returns the value structure as a dictionary.

        Jinja template strings are used as placeholders for *DictletAssembly* values. They are present as a key/value pairs:

         - a dict key representing the id (dictlet_type+'.'+id) of a *DictletAssembly* shows the target location for the result
             of a *DictletAssembly* object
         - a Python structure (usually a dict, list, or templated string) using '__result__' within as placeholder for the
             value of a *DictletAssembly* object indicating the schema of the result of said value. In cases where no schema
             was provided, this will be a single string with the value '__result__'. Also, in some cases this schema might
             miss some potential parts of the result, as the *DictletAssembly* values can be dynamically typed. In those cases
             a runtime error will be thrown if a property is requested but not present.

        """
        return self._structure

    @property
    def _structure(self):

        if self._structure_value is not None:
            return self._structure_value

        self._structure_value = self._assemble_structure()
        return self._structure_value

    @property
    def dictlet_type_names(self) -> List[str]:
        """Returns a list of all *Dictlet* type names that are involved with this structures list of *DictletAssembly*-s."""

        return self._dictlet_type_names

    @property
    def dictlet_ids(self) -> List[str]:
        """Returns a list of all *DictletAssembly* ids that are involved with this structures list of *DictletAssembly*-s."""

        return self._dictlet_ids

    def dictlet_ids_for_type(self, dictlet_type_name: str) -> List[str]:

        return self._dictlet_ids_per_type.get(dictlet_type_name, [])

    def _assemble_structure(self) -> Dict[str, Any]:
        _structure = {}
        for dictlet in self._dictlets:

            target = dictlet.target

            # first time we replace potential result schemas
            result_schema = dictlet.meta.result

            parent_node = None

            if not target:
                parent_node = _structure
                target_name = ""
            else:
                if "." not in target:
                    parent_node = _structure
                    target_name = target
                else:
                    target_parent, target_name = target.split(".", 1)

                    try:
                        parent_node = dpath.util.get(
                            _structure, target_parent, separator="."
                        )
                    except (KeyError, IndexError):
                        pass

                    if parent_node is None:

                        # this is the easiest case, we just add the result schema
                        dpath.util.new(_structure, target_parent, {}, separator=".")
                        parent_node = dpath.util.get(
                            _structure, target_parent, separator="."
                        )

            # print(parent_node)
            dictlet_id_template_string = "{{{{ {}.{} }}}}".format(
                dictlet.dictlet_type_name, dictlet.id
            )

            if dictlet_id_template_string in parent_node.keys():
                raise Exception(
                    "DictletAssembly value already present in structure, this is a bug."
                )

            if not target_name:
                parent_node[dictlet_id_template_string] = result_schema
            else:
                parent_node.setdefault(target_name, {})[
                    dictlet_id_template_string
                ] = result_schema

        return _structure

    def get_dependencies_for_query(self, query: str = None) -> Set[str]:
        """Return a set of dependency *DictletsDictlet* that need to have resolved before this query can be executed.

        Basically, this figures out which *DictletsDictlet* need to be resolved in order for the underlying ``dpath`` library
        to execute a query on 'real' (non-templated) values.

        - *query*: a (dpath) query (with or without globs), if empty, '*' will be used
        """
        if not query:
            query = "*"

        # if not isinstance(query, six.string_types):
        #     raise TypeError("Query must be a string: {}".format(query))

        if query == "*":

            schema = get_template_schema(self._structure)
            schema.pop(
                "__result__", None
            )  # for this purpose we don't need to be concerned about the result schema
            deps = set()

            for dictlet_type_name, ids in schema.items():
                if dictlet_type_name not in self.dictlet_type_names:
                    raise ValueError(
                        "Invalid query, dictlet type '{}' not available, must be one of: {}".format(
                            dictlet_type_name, self.dictlet_type_names
                        )
                    )
                for id in ids.keys():
                    # id = assemble_dictlet_id(dictlet_type_name, id)
                    if id not in self.dictlet_ids:
                        raise ValueError(
                            "Invalid query, no dictlet '{}' in dictlet type '{}', possible ids: {}".format(
                                id,
                                dictlet_type_name,
                                self.dictlet_ids_for_type(dictlet_type_name),
                            )
                        )
                    deps.add(id)

            return deps

        if query_is_glob(query):
            query_results = dpath.util.search(
                self.to_dict(), query, separator=".", yielded=True
            )
            deps = set()
            for qr in query_results:

                d = self.get_dependencies_for_query(qr[0])
                deps.update(d)
                d = self.get_dependencies_for_query(qr[1])
                deps.update(d)

            return deps

        match = True
        current = self._structure
        current_path = []

        if isinstance(query, six.string_types):
            tokens = query.split(".")
        elif isinstance(query, Mapping):
            tks = get_template_keys(query.keys(), jinja_env=DEFAULT_DICTLETS_JINJA_ENV)
            if not tks:
                return set()
            else:
                schema = get_template_schema(query)
                leaves = get_leaves_from_dict(schema)
                deps = set()
                for l in leaves:
                    if l[0] not in self.dictlet_type_names:
                        raise ValueError(
                            "Invalid dictlet type token '{}' encountered in query '{}', must be one of: {}".format(
                                l[0], query, self.dictlet_type_names
                            )
                        )
                    id = l[1]  # assemble_dictlet_id(l[0], l[1])
                    if id not in self.dictlet_ids:
                        raise ValueError(
                            "Invalid id token '{}' encountered in query '{}', must be one of: {}".format(
                                l[1], query, self.dictlet_ids_for_type(l[0])
                            )
                        )
                    deps.add(id)
                return deps
        else:
            tokens = [query]

        # first, check whether we can find the full path without any templates
        for token in tokens:

            current_path.append(token)

            if not isinstance(current, Mapping) or current.get(token, None) is None:
                break

            if token in current.keys():
                current = current[token]
                continue

            tks = get_template_keys(
                current.keys(), jinja_env=DEFAULT_DICTLETS_JINJA_ENV
            )
            if tks:
                # this means we've hit a *DictletAssembly* result placeholder
                # we don't need to go any further down, because the (only) next element will be the schema
                # associated with this particular *DictletAssembly* result
                schema = get_template_schema(current)
                leaves = get_leaves_from_dict(schema)
                deps = set()
                for l in leaves:
                    if l[0] not in self.dictlet_type_names:
                        raise ValueError(
                            "Invalid dictlet type token '{}' encountered in query '{}', must be one of: {}".format(
                                l[0], query, self.dictlet_type_names
                            )
                        )
                    id = l[1]  # assemble_dictlet_id(l[0], l[1])
                    if id not in self.dictlet_ids:
                        raise ValueError(
                            "Invalid id token '{}' encountered in query '{}', must be one of: {}".format(
                                l[1], query, self.dictlet_ids_for_type(l[0])
                            )
                        )
                    deps.add(id)
                return deps

            # being here means we have a dict that could contain *DictletAssembly* placeholders

            if not isinstance(current, (Mapping, Dictionary)):

                raise ValueError(
                    "Value for query path parent '{}' not a dict: {}".format(
                        ".".join(current_path[0:-1]), current
                    )
                )

            match = False
            for k in current.keys():
                if token == k:
                    match = True
                    break
            if match:
                current = current[token]
                continue
            else:
                match = False
                break

        if match:
            # means we found a direct value for our path
            schema = get_template_schema(current)
            schema.pop(
                "__result__", None
            )  # for this purpose we don't need to be concerned about the result schema
            deps = set()
            for dictlet_type_name, ids in schema.items():
                if dictlet_type_name not in self.dictlet_type_names:
                    raise ValueError(
                        "Invalid dictlet type token '{}' for query '{}', must be one of: {}".format(
                            dictlet_type_name, query, self.dictlet_type_names
                        )
                    )

                for id in ids.keys():
                    # id = assemble_dictlet_id(dictlet_type_name, id)
                    if id not in self.dictlet_ids:
                        raise ValueError(
                            "Invalid id token '{}' in query '{}', must be one of: {}".format(
                                id, query, self.dictlet_ids_for_type(dictlet_type_name)
                            )
                        )
                    deps.add(id)

            return set(deps)

        # we didn't find a 'direct' hit, so now check for keys that might be results of dictlets
        deps = set()

        current = self._structure
        current_path = []
        for token in tokens:

            current_path.append(token)
            if not isinstance(current, (Mapping, Dictionary)):
                raise ValueError(
                    "Value for query '{}' not a dict: {}".format(
                        ".".join(current_path), current
                    )
                )

            vals = {}

            for key in current.keys():
                if string_is_templated(key, jinja_env=DEFAULT_DICTLETS_JINJA_ENV):
                    vals[key] = current[key]

            # TODO: check against schema
            if not vals:
                match = False
                for k in current.keys():
                    if token == k:
                        match = True
                        break
                if match:
                    current = current[token]
                    continue

            schema = get_template_schema(vals)
            schema.pop(
                "__result__", None
            )  # for this purpose we don't need to be concerned about the result schema

            for dictlet_type_name, ids in schema.items():

                if dictlet_type_name not in self.dictlet_type_names:
                    raise ValueError(
                        "Invalid dictlet type token '{}' for query path '{}', must be one of: {}".format(
                            dictlet_type_name,
                            ".".join(current_path),
                            self.dictlet_type_names,
                        )
                    )

                for id in ids.keys():
                    # id = assemble_dictlet_id(dictlet_type_name, id)
                    if id not in self.dictlet_ids:
                        raise ValueError(
                            "Invalid id token '{}' for query path '{}', must be one of: {}".format(
                                id,
                                ".".join(current_path),
                                self.dictlet_ids_for_type(dictlet_type_name),
                            )
                        )

                    deps.add(id)

        return deps

    def __repr__(self):

        return "[AssemblyStructure: {}]".format(readable(self.to_dict(), out="pformat"))
